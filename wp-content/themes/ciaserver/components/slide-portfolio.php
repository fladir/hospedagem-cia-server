<div class="owl-carousel owl-theme" id="bianca-portfolio">
    <?php
    $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];

    if(is_single()) {
        $args = array(
            'post_type'         => 'portfolio',
            'posts_per_page'    => -1,
            'post__not_in'       => array( $post->ID )
        );
    } else {
        $args = array(
            'post_type' => 'portfolio',
            'orderby'   => 'post_date',
            'order'     => 'DESC',
            'posts_per_page' => -1,
        );
    }

    $query = new WP_Query($args);
    $count = 1;
    ?>
    <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
            <?php $listas = get_field('materiais_usados', $query->id); ?>
            <div class="item">
                <!-- Dados do portfólio -->
                <div class="col-12 col-lg-6 my-130 order-1 order-lg-0">
                    <div class="offset-xl-2">
                        <div class="ml-md-5">
                            <h3><span><?php echo $count; ?>.</span> <?php the_title(); ?></h3>
                            <p class="truncate3">

                                <?php echo substr(get_the_excerpt(), 0, 300); ?>...

                            </p>
                            <?php if ($listas) : ?>
                                <h4><?php echo get_field('titulo_materiais'); ?></h4>
                                <ul>
                                    <?php foreach ($listas as $key => $lista) : ?>
                                        <li><?php echo $lista['material_utilizado']; ?></li>
                                        <?php if ($key == 2) {
                                            break;
                                        } ?>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                            <a href="<?php the_permalink(); ?>" title="<?php get_the_title(); ?>">Ver projeto</a>
                        </div>
                    </div>
                </div>

                <!-- Imagem do portfólio -->
                <div class="col-12 col-lg-6 my-130 slide-portfolio pr-0 order-0 order-lg-1">

                    <?php if (has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail('destaque-portfolio', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                    <?php else :  ?>
                        <?php echo wp_get_attachment_image($img_padrao, 'destaque-portfolio', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                    <?php endif; ?>

                </div>
                <div class="order-3" data-dot="<button role='button' class='owl-dot'><?php echo $count; ?></button>"></div>
                <?php ++$count; ?>
            </div>
    <?php endwhile;
    endif; ?>
    <?php wp_reset_postdata(); ?>

</div>