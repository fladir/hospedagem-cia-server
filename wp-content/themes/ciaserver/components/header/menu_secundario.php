<?php
$logo = get_theme_mod('custom_logo');
$logo_alt = get_theme_mod('alter_logo');

$header_type = get_theme_mod('header_logo');
?>
<div id="navbarWapper">
    <nav id="menu-secundario" class="navbar navbar-expand-xl menu-absolute animated fadeIn" role="navigation">

        <div class="container">

            <a class="navbar-brand" href="<?php bloginfo('url'); ?>" title="<?php echo bloginfo('name'); ?>">
                <?php if (has_custom_logo()) : ?>
                    <?php if($header_type === 'logo') : ?>
                        <img src="<?php echo wp_get_attachment_image_url($logo, 'logo'); ?>"
                         alt="<?php echo get_bloginfo('name'); ?>">
                    <?php elseif ($header_type === 'logo-alt') : ?>
                        <img src="<?php echo wp_get_attachment_image_url($logo_alt, 'logo'); ?>"
                             alt="<?php echo get_bloginfo('name'); ?>">
                    <?php endif; ?>
                <?php else: ?>
                    <h1><?php bloginfo('name'); ?></h1>
                <?php endif; ?>
            </a>

            <?php if (is_home() || is_front_page()) : ?>
                <h1 class="sr-only"><?php echo bloginfo('name') . ' | ' . bloginfo('description'); ?></h1>
            <?php endif; ?>

            <?php
            wp_nav_menu( array(
                'theme_location'    => 'primary',
                'depth'             => 1,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse justify-content-end d-none d-xl-block',
                'container_id'      => 'menu-secundario',
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker(),
            ) );
            ?>


        </div>
        <div class="nav-toggler menuIcon2 d-block d-xl-none">
            <i class="fas fa-bars"></i>
            <p>Menu</p>
        </div>
    </nav>
</div>
<?php wp_enqueue_script('scrollNavbar', get_template_directory_uri() . '/components/header/scrollNavbar.js', array('jquery'), 1, true); ?>
<?php get_template_part('components/header/menu_mobile'); ?>
<div id="navTogglerFixed" class="nav-toggler menuIcon3 d-block d-xl-none">
    <span></span>
</div>
