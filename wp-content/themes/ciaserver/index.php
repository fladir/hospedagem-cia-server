<?php
get_header();
?>
<?php get_template_part('/components/header/header-padrao'); ?>

<section class="main-blog">
    <div class="container">
        <div class="row">
            <!-- Posts -->
            <div class="col-12 col-lg-8">
                <?php get_template_part('/components/blog/blog'); ?>
            </div>
            <!-- Sidebar -->
            <aside class="col-12 col-lg-3 offset-lg-1 wow fadeInRight">
                <!-- Pesquisar e categoria -->
                <?php dynamic_sidebar( 'sidebar-right' ); ?>
                
                <!-- Mais lidos e banners -->
                <?php get_template_part( '/components/blog/sidebar' ); ?>
            </aside>
        </div>
    </div>
</section>

<?php get_footer(); ?>