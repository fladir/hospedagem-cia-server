<?php $bannerHorizontal = get_field('anuncios_paginas_internas', 'options')['anuncio_banner_horizontal']; ?>
<?php if ($bannerHorizontal) : ?>
    <?php foreach ($bannerHorizontal as $key => $banner) : ?>
        <div class="row mt-5">
            <a href="<?php echo $banner['link_banner_horizontal']; ?>">
                <figure>
                    <img src="<?php echo wp_get_attachment_image_url($banner['imagem_banner_horizontal'], 'banner-horizontal'); ?>" alt="<?php echo get_the_title(); ?>">
                </figure>
            </a>
        </div>
    <?php endforeach; ?>
<?php endif; ?>