<?php $infos_iniciais = get_field('info_bloco_um');
if ($infos_iniciais) :
    $count = 0;
?>
    <div class="container">
        <?php foreach ($infos_iniciais as $key => $info_inicial) : ?>
            <div class="info-2cols row align-items-center my-5 <?php echo $count % 2 ? '' : 'flex-md-row-reverse'; ?>">

                <div class="col-12 col-md-6 mask-border">
                    <div class="msk"></div>
                    <div class="clip-interno">
                        <div class="owl-carousel owl-theme" id="catalogo-imagens-<?php echo $count; ?>">
                            <?php $slides = $info_inicial['imagem_destaque_inicial']; ?>
                            <?php foreach ($slides as $slide) : ?>
                                <img src="<?php echo wp_get_attachment_image_url($slide['banner_destaque_inicial_vitrine'], 'blog-destaque'); ?>" alt="<?php echo $info_inicial['titulo_secao_inicial']; ?>">
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <script>
                        window.addEventListener('DOMContentLoaded', event => {
                            instanciarPostsSlide('#catalogo-imagens-<?php echo $count; ?>');
                        });
                    </script>

                </div>

                <div class="col-12 col-md-6">
                    <h2><span><?php echo $info_inicial['titulo_secao_inicial']; ?></span></h2>
                    <p><?php echo $info_inicial['texto_secao_inicial']; ?></p>
                </div>
            </div>
            <?php $count++; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>