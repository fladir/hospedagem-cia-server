<?php $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao']; ?>
<?php $term = get_queried_object(); ?>
<?php $banner = get_field('banner_topo_categoria'); ?>
<?php $resumo = $term->description; ?>
<section class="header-padrao">
    <figure>
        <?php if(!empty($banner)) : ?>
            <img src="<?php echo wp_get_attachment_image_url( $banner, 'header-padrao'); ?>" alt="<?php echo $term->name; ?>">
        <?php else: ?>
            <img class="d-block w-100" src="<?php echo wp_get_attachment_image_url($img_padrao, 'header-padrao'); ?>" alt="<?php echo $term->name; ?>">
        <?php endif; ?>
    </figure>
    <div class="container h-100">
        <div class="row align-items-center h-100">
            
            <div class="col-12">
                <h1><?php echo $term->name; ?></h1>

                <?php if($resumo) : ?>
                    <p class="w-50 d-none d-md-block"><?php echo $resumo; ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>