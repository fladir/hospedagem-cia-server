<?php
    $banner = get_field('imagem_call_to_action');
?>
<?php if($banner) : ?>
<section class="cta-home">
    <a href="#modal-busca" data-toggle="modal">
        <img src="<?php echo wp_get_attachment_image_url($banner, 'call-to-action'); ?>" alt="<?php echo bloginfo('name'); ?>">
    </a>
</section>
<?php endif; ?>
