<?php
get_header();
$term = get_queried_object();
?>

<?php get_template_part('components/header/header-padrao'); ?>


<!-- Destaque da página de empreendimentos -->
<?php
$args = array(
    'post_type' => 'empreendimentos',
    'posts_per_page'    =>  -1,
    'orderby'   => 'date',
    'order' => 'DESC',
    'tax_query' => array(
        array(
            'taxonomy' => 'categoria-empreendimentos',
            'field' => 'slug',
            'terms' => $term->slug,
        )
    )
);
$WPQuery = new WP_Query($args);


?>

<?php if($WPQuery->found_posts >= 1) : ?>
    <section class="lancamento-home empreendimentos-page">
        <!--  Box Verde  -->
        <div class="background-wrap">
            <div class="background"></div>
        </div>
        <!--  Imagem que sobrepõe parte do slider  -->
        <div class="img-bg">
            <img class="rellax" data-rellax-speed="2" src="<?php echo bloginfo('template_directory'); ?>/assets/img/img-background.png" />
        </div>

        <div class="container">
            <div class="row align-items-center">

                <?php if($WPQuery->have_posts()) : while($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                    <?php
                    $img_destaque = get_field('imagem_destaque');
                    $listas = get_field('lista_diferenciais');
                    ?>
                    <div class="col-12 col-md-6 wow fadeIn d-flex align-items-center lancamento-container">
                        <div class="box-lancamento">
                            <a href="<?php the_permalink(); ?>">

                                <figure>
                                    <?php if ($img_destaque) : ?>
                                        <img class="rounded" src="<?php echo wp_get_attachment_image_url($img_destaque, 'destaque-lancamento'); ?>" alt="<?php echo get_the_title(); ?>">
                                    <?php else : ?>
                                        <img class="rounded" src="<?php echo wp_get_attachment_image_url($img_padrao, 'destaque-lancamento'); ?>" alt="<?php echo get_the_title(); ?>">
                                    <?php endif; ?>
                                </figure>
                                <!-- Informações Exibidas sem Hover -->
                                <div class="texto-info">
                                    <h3 class="truncate3"><?php echo get_the_title(); ?></h3>
                                    <span> <?php echo $listas[0]['diferencial']; ?></span>
                                </div>

                                <!-- Informações Exibidas com Hover -->
                                <div class="info-hover">
                                    <h3 class="truncate3 text-center"><?php echo get_the_title(); ?></h3>


                                    <div class="pl-29">
                                        <span> <?php echo get_field('local_empreendimento'); ?> </span>

                                        <?php if($listas) : ?>
                                            <ul>
                                                <?php foreach ($listas as $key => $lista) : ?>
                                                    <li><?php echo $lista['diferencial']; ?></li>
                                                    <?php if($key >= 3){ break; } ?>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                    </div>


                                    <div class="btn btn-lancamentoDiv">Conheça</div>
                                </div>

                            </a>
                        </div>
                    </div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
        </div>

    </section>
<?php endif; ?>

<?php get_footer(); ?>
