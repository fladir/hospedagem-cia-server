<?php
    $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
?>

<section class="contato-home">
    <img class="img-bg" src="<?php echo wp_get_attachment_image_url(get_field('imagem_fundo_contato'), 'contato-form'); ?>" alt="<?php echo get_field('titulo_contato'); ?>">
    <div class="container">
        <div class="row">

            <!-- Formulário -->
            <div class="col-12 col-md-6 offset-md-5 col-lg-6 offset-lg-7 form-box d-flex flex-column justify-content-center align-items-center text-center">
                <h2><?php echo get_field('titulo_contato'); ?></h2>
                <?php echo do_shortcode('[contact-form-7 id="5" title="Contato"]'); ?>
            </div>
            
        </div>
    </div>

</section>