<form method="get" role="search" action="<?php echo esc_url(home_url('/')); ?>">
    <div class="form-row align-items-center">
        <div class="col-auto my-1">
            <h6 class="m-0">Encontre seu imóvel</h6>
        </div>
        <div class="col my-1">
            <select name="cidade_emp" id="inlineFormCustomSelect">
                <option selected>Cidade</option>
                <?php
                $args = array(
                    'taxonomy' => 'categoria-empreendimentos',
                );
                $cidades = get_terms($args);
                ?>
                <?php foreach ($cidades as $cidade) : ?>
                    <option value="<?php echo $cidade->term_id; ?>"><?php echo $cidade->name; ?></option>
                <?php endforeach; ?>
            </select>

        </div>
        <div class="col my-1">
            <select name="tipo_emp" id="inlineFormCustomSelect">
                <option selected>Estágio da obra</option>

                <?php
                $conds = get_field_object('field_5f1b3c3672a6d');

                $comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');

                $semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');
                ?>
                <?php foreach ($conds['choices'] as $cond) : ?>

                    <option value="<?php echo strtolower(str_replace($comAcentos, $semAcentos, $cond)); ?>"><?php echo $cond; ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <!-- PASSING THIS TO TRIGGER THE ADVANCED SEARCH RESULT PAGE FROM functions.php -->
        <input type="hidden" name="search" value="empreendimentos">

        <div class="col-auto my-1">
            <button type="submit" class="btn btn-form mt-0 py-1"><i class="fas fa-search"></i></button>
        </div>
    </div>
</form>