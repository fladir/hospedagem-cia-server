
<?php $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
    $term = get_queried_object();
?>
<section class="header-padrao">
    <!-- Lente branca -->
    <div class="box-white"></div>

    <!-- Lente escura -->
    <div class="box-dark"></div>

    <!-- Imagem do banner-->
    <figure>
        <!-- Imagem destacada do blog -->
        <?php if(is_home() || is_category()) : ?>
            <?php $img_blog = get_the_post_thumbnail( get_option( 'page_for_posts' )); ?>

            <?php if($img_blog) : ?>
                <?php echo apply_filters( 'post_thumbnail_html', get_the_post_thumbnail( get_option( 'page_for_posts' ), 'header-padrao')  ); ?>
            <?php else: ?>
                <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'header-padrao'); ?>" alt="<?php echo get_the_title(); ?>">
            <?php endif; ?>

        <!-- Img destacada para taxonomia -->
        <?php elseif(is_tax('categoria-empreendimentos')) : ?>
            <?php $img_cat = get_page_by_path( 'empreendimentos', OBJECT, 'page' ); ?>
            <?php if($img_cat) : ?>
                <?php echo get_the_post_thumbnail( $img_cat , 'header-padrao'); ?>
            <?php else: ?>
                <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'header-padrao'); ?>" alt="<?php echo get_the_title(); ?>">
            <?php endif; ?>

        <!-- Se for outra página interna -->
        <?php else: ?>
            <?php if(has_post_thumbnail()) : ?>
                <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'header-padrao'); ?>" alt="<?php echo get_the_title(); ?>">
            <?php else: ?>
                <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'header-padrao'); ?>" alt="<?php echo get_the_title(); ?>">
            <?php endif; ?>
        <?php endif; ?>
    </figure>


    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Se for a single do Blog ou a Página do Blog -->
                <?php if(is_home()): ?>
                    <h1><?php echo apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) ); ?></h1>
                <!-- Categoria do blog -->
                <?php elseif(is_category() || is_tax()): ?>
                    <h1><?php echo $term->name; ?></h1>
                <!-- Se for página de busca -->
                <?php elseif(is_search()) : ?>
                    <h1>Resultado da busca</h1>
                <!-- Se for a página 404-->
                <?php elseif(is_404()) : ?>
                    <h1>Página não encontrada</h1>
                <!-- Se for outra página interna -->
                <?php else: ?>
                    <h1><?php the_title(); ?></h1>
                <?php endif; ?>
                <?php if (function_exists('yoast_breadcrumb')) {
                    yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
                } ?>

                <?php if(is_page('empreendimentos') || is_tax('categoria-empreendimentos')) : ?>
                    <p>Escolha também por status do projeto:</p>
                    <form>
                        <div class="form-group">
                            <select class="form-control" id="status-obra" onchange="window.location.href=this.value">
                                <option>Escolha o estágio da obra</option>
                                <?php
                                    $args = array(
                                        'taxonomy'  => 'categoria-empreendimentos',
                                        'hide_empty' => 0,
                                    );
                                    $categories = get_categories($args);
                                ?>
                                <?php foreach ($categories as $category) : ?>
                                <option value="<?php echo get_category_link($category->term_id); ?>"><?php echo $category->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </form>
                <?php endif; ?>
            </div>
        </div>

    </div>
</section>
