
<?php $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
    $term = get_queried_object();
?>
<section class="header-padrao">
    <!--  Box de fundo 100% opaco -->
    <div class="bg-header"></div>

    <!-- Imagem do banner-->
    <figure>
        <!-- Imagem destacada do blog -->
        <?php if(is_home() || is_category() ) : ?>
            <?php $img_blog = get_the_post_thumbnail( get_option( 'page_for_posts' )); ?>

            <?php if($img_blog) : ?>
                <?php echo apply_filters( 'post_thumbnail_html', get_the_post_thumbnail( get_option( 'page_for_posts' ), 'header-padrao')  ); ?>
            <?php else: ?>
                <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'header-padrao'); ?>" alt="<?php echo get_the_title(); ?>">
            <?php endif; ?>

        <!-- Se for outra página interna -->
        <?php else: ?>
            <?php if(has_post_thumbnail()) : ?>
                <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'header-padrao'); ?>" alt="<?php echo get_the_title(); ?>">
            <?php else: ?>
                <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'header-padrao'); ?>" alt="<?php echo get_the_title(); ?>">
            <?php endif; ?>
        <?php endif; ?>
    </figure>

    <!--  Box de fundo semitransparente -->
    <div class="bg-banner"></div>

    <div class="row">
        <div class="container-fluid">
            <div class="col-12">
                <h1>Empreendimentos disponíveis</h1>
            </div>
        </div>

    </div>
</section>
