jQuery(document).ready(function(){
    jQuery('.date').mask('99/99/9999');
    jQuery('.phone').mask('(99) 9999-9999');
    jQuery('.cel').mask('(99) 9 9999-9999');
});

enviarMensagem = () => {
    let nome = document.querySelector('#nomeProfissional').value;
    let aniversario = document.querySelector('#aniversarioProfissional').value;
    let tel = document.querySelector('#telProfissional').value;
    let cel = document.querySelector('#celularProfissional').value;
    let email = document.querySelector('#emailProfissional').value;
    let end = document.querySelector('#enderecoProfissional').value;
    let bairro = document.querySelector('#bairroProfissional').value;
    let cidade = document.querySelector('#cidadeProfissional').value;
    let estado = document.querySelector('#estadoProfissional').value;
    let atividade = document.querySelector('#atividadeProfissional').value;
    let mensagem = document.querySelector('#mensagemAtividade').value;
    let dados = document.querySelector('#contaProfissional').value;
    let obs = document.querySelector('#mensagemAdicional').value;
    if(nome == '' || aniversario == '' || cel == '' || end == '' || estado == '' || atividade == '' || email == '' || tel == '' || bairro == '' || cidade == '' || mensagem == ''){
        jQuery('#contato-completo').append('<div class="alert alert-danger alert-dismissible mt-3 fade show" role="alert"> Os campos com * são de preenchimento obrigatório <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        setTimeout(() => {
            jQuery('.alert').css('opacity', 0);
        }, 5000);
        setTimeout(() => {
            jQuery('.alert').css('display', 'none');
        }, 6000);
    } else {
        let loading = '<div class="spinner-border ml-3" role="status"><span class="sr-only">Loading...</span></div>'
        let mensagemSucesso = '<div class="alert alert-success mt-3 fade show" role="alert"> Mensagem enviada com sucesso <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
        let mensagemErro = '<div class="alert alert-danger mt-3 fade show" role="alert"> Ocorreu um erro, tente novamente mais tarde! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'

        let formData = jQuery('#contato-profissional').serialize(); 
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            dataType: 'json',
            data: 'action=enviarMensagemProfissional&' + formData,
            beforeSend: function () {
                jQuery('#btnSubmit').append(loading);
            },
            complete: function () {
                jQuery('#btnSubmit').html('Enviar Mensagem')
            },
            success: function (resposta) {                                           
                if (resposta.status == 200) {                    
                    jQuery('#contato-profissional').append(mensagemSucesso);
                    setInterval(window.location.href = resposta.url,3000);
                } else {
                    jQuery('#contato-profissional').append(mensagemErro);
                    setTimeout(() => {
                        jQuery('.alert').css('opacity', 0);
                    }, 5000);
                    setTimeout(() => {
                        jQuery('.alert').css('display', 'none');
                    }, 6000);
                }
            }
        })
    }
}