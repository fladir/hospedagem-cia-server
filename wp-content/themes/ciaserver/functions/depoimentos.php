<?php
#Empreendimentos
$args = array(
    'show_in_rest' => true,
    'supports'  => array('title', 'editor'),
    'menu_icon' => 'dashicons-format-quote',
    'rewrite' => array('slug' => 'depoimentos'),
);

$labels = array(
    'name' => 'Depoimentos',
    'singular_name' => 'Depoimento',
    'menu_name' => 'Depoimentos',
    'add_new' => 'Adicionar novo ' . strtolower('Depoimento'),
    'add_new_item' => 'Adicionar novo ' . strtolower('Depoimento'),
    'new_item' => 'Novo ' . strtolower('Depoimento'),
    'edit_item' => 'Editar ' . strtolower('Depoimento'),
    'view_item' => 'Vizualizar ' . strtolower('Depoimentos'),
    'all_items' => 'Todos os ' . strtolower('Depoimentos'),
    'search_items' => 'Pesquisar ' . strtolower('Depoimentos'),
    'not_found' => 'Nenhum ' . strtolower('Depoimento') . ' foi encontrado',
    'not_found_in_trash' => 'Nenhum ' . strtolower('Depoimento') . ' encontrado na lixeira',
    'featured_image' => 'Imagem destacada',

    'set_freatured_image' => 'Escolher como imagem destacada',
    'remove_featured_image' => 'Remover imagem destacada',
    'use_freatured_image' => 'Usar como imagem descatada'

);
$custom_post_type_slide = new pbo_register_custom_post_type('depoimentos', 'Depoimentos', $args, $labels);