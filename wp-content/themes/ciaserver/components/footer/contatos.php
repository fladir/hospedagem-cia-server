<?php $rodape = get_field('grupo_footer', 'options'); ?>
<?php $contatos = get_field('grupo_informacoes_para_contato', 'options'); 
$telefones = $contatos['telefones'];
$whatsapp = $contatos['whatsapp'];
$emails = $contatos['emails'];
$enderecos = $contatos['enderecos'];
?>
<div class="row">
    <div class="col-12">
        <h4>Fale com a gente</h4>
        <?php if($enderecos) : ?>
            <?php foreach ($enderecos as $key => $end) : ?>
                <span><?php echo $end['endereco']; ?></span>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if($telefones || $emails || $whatsapp) : ?>
            <ul>
            <?php if($telefones) : ?>
                <?php foreach($telefones as $key => $fone) : ?>
                    <li>
                        <a href="tel:<?php echo $fone['numero_telefone']; ?>" target="_blank">
                            <?php echo $fone['numero_telefone']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if($whatsapp) : ?>
                <?php foreach($whatsapp as $key => $whats) : ?>
                    <li>
                        <a href="https://api.whatsapp.com/send?phone=55<?php echo $whats['link_whatsapp']; ?>" target="_blank">
                            <?php echo $whats['numero_whatsapp']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if($emails) : ?>
                <?php foreach($emails as $key => $mail) : ?>
                    <li>
                        <a href="mailto:<?php echo $mail['endereco_email']; ?>" target="_blank">
                            <?php echo $mail['endereco_email']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
            </ul>
        <?php endif; ?>
    </div>

</div>
