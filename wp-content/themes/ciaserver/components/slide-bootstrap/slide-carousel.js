jQuery(document).ready(function () {
    slideCarosuel();
});

const slideCarosuel = () => {
    jQuery('#slide-home').owlCarousel({
        loop: true,
        autoplay: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        margin: 0,
        nav: false,
        dots: true,
        items: 1,
    })
}