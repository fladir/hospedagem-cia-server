<?php
get_header();

$itens = get_field('lista_diferenciais');
?>
<!-- Header padrão -->
<?php get_template_part('components/header/header-padrao'); ?>

<!-- Seção CONHEÇA -->
<section class="lancamento-home single-empreendimento">
    <!--  Box Verde  -->
    <div class="background-wrap">
        <div class="background"></div>
    </div>
    <!--  Imagem que sobrepõe parte do slider  -->
    <div class="img-bg">
        <img class="rellax" data-rellax-speed="2" src="<?php echo bloginfo('template_directory'); ?>/assets/img/img-background.png" />
    </div>

    <div class="container">
        <div class="row lancamento-img">
            <div class="col-12 col-md-6 single-destaque">
                <img class="single-img-destaque" src="<?php echo wp_get_attachment_image_url(get_field('imagem_destaque'), 'destaque-lancamento'); ?>" alt="<?php echo get_the_title(); ?>">

                <div class="texto-info">
                    <h3 class="truncate3"><?php echo get_the_title(); ?></h3>
                    <span><?php echo $itens[0]['diferencial']; ?></span>
                </div>
            </div>

            <!-- Destaque - Quadro Verde -->
            <?php if($itens) : ?>
            <div class="col-12 col-md-6">
                <div class="lancamento-destaque">
                    <h3>Diferenciais</h3>
                    <ul>


                        <?php foreach ($itens as $key => $item) : ?>
                            <li>
                                <?php if($item['icone_empreendimento']) : ?>
                                    <img src="<?php echo wp_get_attachment_image_url($item['icone_empreendimento'], 'icone-lista'); ?>" alt="<?php echo get_the_title(); ?>">
                                <?php endif; ?>
                                <span><?php echo $item['diferencial']; ?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>

</section>


<!-- GALERIA -->
<?php $itens = get_field('itens_galeria'); ?>
<?php if($itens) : ?>
    <section class="galeria">
        <h2 class="text-center">Conheça</h2>

        <div class="row">

            <div class="col-12 col-xl-6 offset-xl-3 slider">

                <div id="galeria-slide" class="owl-carousel owl-theme">

                    <?php foreach ($itens as $key => $item) : ?>
                    <div class="item">
                        <?php if($item['imagem_galeria']) : ?>
                        <figure>
                            <img src="<?php echo wp_get_attachment_image_url($item['imagem_galeria'], 'galeria-empreendimento'); ?>" alt="<?php echo get_the_title(); ?>" />
                        </figure>
                        <?php elseif($item['video']) : ?>
                            <?php echo $item['video']; ?>
                        <?php endif; ?>
                    </div>
                    <?php endforeach; ?>
                </div>

            </div>

        </div>
    </section>
<?php endif; ?>

<!-- Conteúdo da página -->
<?php if(get_post()->post_content): ?>
    <section class="page-default conteudo-empreendimento">
        <div class="container">
            <div class="row">
                <div class="col-12"><?php the_content(); ?></div>
            </div>
        </div>
    </section>
<?php endif; ?>


<!-- Ficha técnica e Status Obra -->
<?php
$mes_status = get_field('mes_obra');
$ano_obra = get_field('ano');
$status_previsao = get_field('status');
$status_real = get_field('status_real_obra');

$fichas = get_field('informacoes_tecnicas');
?>
<?php if($fichas && $status_previsao && $status_real) : ?>
<section class="ficha-status">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-12 col-md-6 ficha-info">

                <div class="col-12 p-0 col-md-10 col-lg-8 offset-lg-4">
                    <h2>Ficha técnica</h2>
                    <?php foreach ($fichas as $key => $ficha) : ?>
                        <p><span><?php echo $ficha['titulo_ficha']; ?></span> <?php echo $ficha['descricao_ficha']; ?></p>
                    <?php endforeach; ?>
                </div>

            </div>
            <div class="col-12 col-md-6 status-info">

                <div class="col-12 p-0 col-md-10 col-lg-7">
                    <h2>Status da obra</h2>
                    <h3><?php echo $mes_status; ?> <?php echo $ano_obra; ?></h3>
                    <?php if($status_previsao) : ?>
                        <p>Status previsto da obra</p>
                        <div class="row mt-2 mb-5">
                            <div class="col-2 col-md-1 numeros"><p><span class="count"><?php echo $status_previsao; ?></span>%</p></div>
                            <div class="col-10 col-md-11">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $status_previsao; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $status_previsao; ?>%"></div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($status_real) : ?>
                        <p>Status Real da obra</p>
                        <div class="row mt-2 mb-5">
                            <div class="col-2 col-md-1 numeros"><p><span class="count"><?php echo $status_real; ?></span>%</p></div>
                            <div class="col-10 col-md-11">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $status_real; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $status_real; ?>%"></div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php $estrutaras = get_field('dados_estruturais'); ?>
                    <?php if($estrutaras) : ?>
                        <!-- Lista de etapas da obra -->
                        <div class="box-etapas">
                            <?php foreach ($estrutaras as $key => $estrutura) : ?>
                                <div class="box">
                                    <!-- Círculo com imagem -->
                                    <div class="circle-wrap">
                                        <div class="circle">
                                            <div class="mask full <?php echo $estrutura['etapa_estrutura']; ?> trigger">
                                                <div class="fill"></div>
                                            </div>
                                            <div class="mask half <?php echo $estrutura['etapa_estrutura']; ?> trigger">
                                                <div class="fill"></div>
                                            </div>
                                            <div class="inside-circle">
                                                <img src="<?php echo wp_get_attachment_image_url($estrutura['icone_estrutura'], 'icone-lista'); ?>" alt="<?php echo $estrutura['etapa_estrutura']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-warp mt-2">
                                        <p class="text-center"><?php echo $estrutura['etapa_estrutura']; ?></p>
                                        <p class="text-center"><span class="count"><?php echo $estrutura['percentual_construido']; ?></span>%</p>
                                    </div>

                                    <style>
                                        .full.<?php echo $estrutura['etapa_estrutura']; ?>,
                                        .<?php echo $estrutura['etapa_estrutura']; ?> fill {
                                            transform: rotate(180deg);
                                        }

                                        .<?php echo $estrutura['etapa_estrutura']; ?>.full.animate,
                                        .<?php echo $estrutura['etapa_estrutura']; ?>.animate .fill {
                                            animation: <?php echo $estrutura['etapa_estrutura']; ?> linear 3s forwards;
                                        }
                                        @keyframes <?php echo $estrutura['etapa_estrutura']; ?> {
                                            0% {
                                                transform: rotate(0deg);
                                            }
                                            100% {
                                                transform: rotate(<?php echo 1.8 * $estrutura['percentual_construido']; ?>deg);
                                            }
                                        }
                                    </style>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<!-- Call-to-action -->
<?php
$args = array(
    'post_type' => 'page',
    'pagename'  => 'home',
);
$WPQuery = new WP_Query($args);
?>
<?php if($WPQuery->have_posts()) : while($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
    <?php get_template_part('components/index/call-to-action'); ?>
<?php endwhile; endif; wp_reset_postdata(); ?>

<!-- Mapa -->
<?php
$mapa = get_field('endereco_do_imovel');
?>
<?php if($mapa) : ?>
    <section class="mapa">
        <div class="box-mapa"><img src="<?php echo bloginfo('template_directory'); ?>/assets/img/location-mapa.png" alt="Mapa" > <h2>Local do imóvel</h2></div>
        <div class="acf-map" data-zoom="<?php echo esc_attr($mapa['zoom']); ?>">
            <div class="marker" data-lat="<?php echo esc_attr($mapa['lat']); ?>" data-lng="<?php echo esc_attr($mapa['lng']); ?>"></div>
        </div>
    </section>
<?php endif; ?>
<?php get_footer(); ?>
