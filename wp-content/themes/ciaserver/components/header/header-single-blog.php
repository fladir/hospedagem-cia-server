<?php $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
$term = get_queried_object();
?>
<section class="header-padrao">
    <!--  Box de fundo 100% opaco -->
    <div class="bg-header"></div>

    <!-- Imagem do banner-->
    <figure>
        <!-- Imagem destacada do blog -->
            <?php $img_blog = get_the_post_thumbnail( get_option( 'page_for_posts' )); ?>

            <?php if($img_blog) : ?>
                <?php echo apply_filters( 'post_thumbnail_html', get_the_post_thumbnail( get_option( 'page_for_posts' ), 'header-padrao')  ); ?>
            <?php else: ?>
                <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'header-padrao'); ?>" alt="<?php echo get_the_title(); ?>">
            <?php endif; ?>

    </figure>

    <!--  Box de fundo semitransparente -->
    <div class="bg-banner"></div>

    <div class="row">
        <div class="container-fluid">
            <div class="col-12">
                <!-- Se for a single do Blog ou a Página do Blog -->
                <h1><?php echo apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) ); ?></h1>
                <?php if (function_exists('yoast_breadcrumb')) {
                    yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
                } ?>
            </div>
        </div>

    </div>
</section>