<section class="posts-relacionados pt-4 pb-5">
    <div class="row">
        <div class="col-12">
            <h3 class="mb-5">Posts relacionados</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-8">
            <div class="row">
                <?php
                $post_item = get_queried_object();
                $cat = get_the_category($post_item->ID);
                $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
                $args = array(
                    'post_type' => 'post',
                    'orderby'     => 'rand',
                    'posts_per_page' => 2,
                    'post__not_in'   => array($post->ID),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'category',
                            'field'    => 'slug',
                            'terms'     => $cat[0]->slug,
                        )
                    )
                );
                $query = new WP_Query($args);
                ?>
                <?php if ($query->have_posts()) : ?>
                    <?php while ($query->have_posts()) : $query->the_post(); ?>
                        <div class="col-12 col-md-6 blog-post wow fadeIn">
                            <a href="<?php the_permalink(); ?>">
                                <figure>
                                    <?php if (has_post_thumbnail()) : ?>
                                        <img class="mb-3" src="<?php echo get_the_post_thumbnail_url($query->ID, 'post-relacionado'); ?>" alt="<?php echo get_the_title(); ?>">
                                    <?php else : ?>
                                        <img class="mb-3" src="<?php echo wp_get_attachment_image_url($img_padrao, 'post-relacionado'); ?>" alt="<?php echo get_the_title(); ?>">
                                    <?php endif; ?>
                                </figure>
                                <p class="truncate4"><?php echo get_the_excerpt(); ?></p>
                            </a>
                            <a class="btn btn-form" href="<?php the_permalink(); ?>" title="<?php get_the_title(); ?>">
                                Leia mais
                            </a>
                        </div>
                        <?php wp_reset_postdata(); ?>
                    <?php endwhile; ?>
                <?php else : ?>
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <h4 class="text-center">Não há outros posts relacionados a categoria atual.</h4>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <!-- banners -->
        <div class="col-12 col-md-4 col-lg-3 offset-lg-1 mt-4 mt-md-0">
            <?php
                $args = array(
                    'post_type' => 'page',
                    'pagename'  => 'blog',
                );
                $WPQuery = new WP_Query($args);
            ?>
            <?php if($WPQuery->have_posts()) : while($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
            <?php $bannersVertical = get_field('anuncio_banner_vertical'); ?>
            
            <?php if ($bannersVertical[1]) : ?>
                <div class="owl-carousel owl-theme" id="banners-relacionados">
            <?php else: ?>
                <div>
            <?php endif; ?>
                <?php foreach ($bannersVertical as $key => $banner) : ?>
                    <!-- Verifica sem tem link -->
                    <?php if ($banner['link_banner_vertical']) : ?>
                        <a class="banner-sidebar" href="<?php echo $banner['link_banner_vertical']; ?>">
                        <?php endif; ?>
                        <!-- Imagem -->
                        <img src="<?php echo wp_get_attachment_image_url($banner['imagem_banner_vertical'], 'post-banner'); ?>" alt="<?php echo get_the_title(); ?>">
                        <!-- Fim tag link -->
                        <?php if ($banner['link_banner_vertical']) : ?>
                        </a>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <?php endwhile; endif; wp_reset_postdata(); ?>
        </div>
    </div>
</section>