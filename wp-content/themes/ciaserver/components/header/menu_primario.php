<div id="menuPrimario" class="wow fadeIn d-none d-xl-block">
    <?php 
    $info_topo = get_field('grupo_informacoes_para_contato', 'options'); 
    $whatsapp = $info_topo['whatsapp'];
    $emails = $info_topo['emails'];
    $telefones = $info_topo['telefones'];
    $midias = $info_topo['redes_sociais'];
    $count = 0; 
    ?>
    <div class="container">

        <div class="row align-items-center">

            <div class="col-12 col-md-9 col-lg-8 text-left container">
                
                <?php if($telefones) : ?>
                    <?php foreach($telefones as $key => $fone) : ?>
                        <span><?php echo $fone['icone_telefone']; ?> <?php echo $fone['numero_telefone']; ?></span>
                        <?php if($count >= 0) { break; } ?>        
                    <?php endforeach; ?>
                <?php endif; ?>
                
                <?php if($whatsapp) : ?>
                    <?php foreach($whatsapp as $whats) : ?>
                        <a href="https://api.whatsapp.com/send?phone=55<?php echo $whats['link_whatsapp']; ?>" target="_blank"><span><i class="fab fa-whatsapp"></i> <?php echo $whats['numero_whatsapp']; ?></span></a> 
                        <?php if($count >= 0) { break; } ?> 
                    <?php endforeach; ?>
                <?php endif; ?>
                
                <?php if($emails) : ?>
                    <?php foreach($emails as $key => $mail) : ?>
                        <span><?php echo $mail['icone_email']; ?> <?php echo $mail['endereco_email']; ?></span>   
                        <?php if($count >= 0) { break; } ?>    
                    <?php endforeach; ?>
                <?php endif; ?>               

            </div>
            <div class="d-none d-md-block col-md-3 col-lg-4 text-right icon-social-topo">
                <?php foreach ($midias as $key => $midia) : ?>
                    <a href="<?php echo $midia['link_social']; ?>" target="_blank"><i class="<?php echo $midia['icone_social']; ?>"></i></a>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
    
</div>
