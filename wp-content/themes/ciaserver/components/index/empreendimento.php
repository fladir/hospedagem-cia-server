<?php
    $args = array(
        'post_type' => 'empreendimentos',
        'posts_per_page'    =>  -1,
        'orderby'   => 'date',
        'order' => 'DESC',
        'meta_query' => array(
            array(
                'key'   => 'destaque_na_home',
                'value' => 0,
            )
        )
    );
    $WPQuery = new WP_Query($args);
?>
<?php if($WPQuery->found_posts >= 1) : ?>
<section class="empreendimentos-home">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex flex-column justify-content-center align-items-center">
                <div id="empreendimentos" class="owl-carousel owl-theme">
                <?php if($WPQuery->have_posts()) : while($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                    <?php
                    $img_destaque = get_field('imagem_destaque');
                    $listas = get_field('lista_diferenciais');
                    ?>
                    <div class="box">
                        <a href="<?php the_permalink(); ?>">

                            <img src="<?php echo wp_get_attachment_image_url($img_destaque,'empreendimento-home'); ?>" alt="<?php echo get_the_title(); ?>">

                            <!-- Informações Exibidas sem Hover -->
                            <div class="texto-info">
                                <h2 class="truncate1"><?php echo get_the_title(); ?></h2>
                                <span> <?php echo $listas[0]['diferencial']; ?></span>
                            </div>

                            <!-- Informações Exibidas com Hover -->
                            <div class="info-hover">
                                <h2 class="truncate3 text-center"><?php echo get_the_title(); ?></h2>


                                <div class="pl-29">
                                    <span> <?php echo get_field('local_empreendimento'); ?> </span>

                                    <?php if($listas) : ?>
                                        <ul>
                                            <?php foreach ($listas as $key => $lista) : ?>
                                                <li><?php echo $lista['diferencial']; ?></li>
                                                <?php if($key >= 2){ break; } ?>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                </div>


                                <div class="btn btn-lancamentoDiv">Conheça</div>
                            </div>

                        </a>
                    </div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
                </div>
            </div>

        </div>
    </div>
</section>
<?php endif; ?>