<?php $imagens = get_field('galeria_de_imagens');
if ($imagens) :
?>
    <section class="galeria-produtos">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="titulo-destaque">Galeria</h2>
                </div>
                <div class="grid">
                    <?php foreach ($imagens as $key => $imagem) : ?>
                        <div class="grid-item">
                            <figure>
                                <?php echo wp_get_attachment_image($imagem['id'], 'portfolio-index', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                            </figure>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php wp_enqueue_script('Masonry-Js', get_template_directory_uri() . '/node_modules/masonry-layout/dist/masonry.pkgd.min.js', array('jquery'), 1, true); ?>
<script>
    jQuery(document).ready(function() {
        let grid = jQuery('.grid').masonry({
            itemSelector: '.grid-item',
            stagger: 30,
        });

        grid.on('click', '.grid-item', function() {
            // change size of item via class
            jQuery(this).toggleClass('grid-full');
            // trigger layout
            grid.masonry();
        });

    });
</script>