window.addEventListener('DOMContentLoaded', (event) => {

    const seta = document.querySelector('#voltarTopo');
    window.addEventListener('scroll', (event) => {
        if (window.scrollY > 300) {
            seta.classList.remove('d-none')
            seta.classList.add('show')
        } else {
            seta.classList.add('d-none')
            seta.classList.remove('show')
        }
    });

    seta.addEventListener('click', (event) => {
        window.scrollTo({ top: 0, behavior: 'smooth' })
    });

});


