<?php /* Template Name: Mensagem Enviada */ ?>
<?php get_header();  ?>
<?php get_template_part('components/header/header-padrao'); ?>
<section class="mensagem-enviada">
    <!--  Box Verde  -->
    <div class="background-wrap">
        <div class="background"></div>
    </div>
    <!--  Imagem que sobrepõe parte do slider  -->
    <div class="img-bg">
        <img class="rellax" data-rellax-speed="2" src="<?php echo bloginfo('template_directory'); ?>/assets/img/img-background.png" />
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 text-center">

                <i class="fas fa-tasks"></i>
                <h1>Obrigado por entrar em contato!</h1>
                <p>Mensagem enviada com sucesso</p>

                <a href="<?php echo home_url() ?>" class="btn btn-form mt-3">Voltar para home</a>
            </div>
        </div>
    </div>
</section>
<?php get_footer() ?>