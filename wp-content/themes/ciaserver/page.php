<?php get_header(); ?>
<?php get_template_part('components/header/header-padrao'); ?>
<section class="page-default wow fadeIn">
    <!--  Box Verde  -->
    <div class="background-wrap">
        <div class="background"></div>
    </div>
    <!--  Imagem que sobrepõe parte do slider  -->
    <div class="img-bg">
        <img class="rellax" data-rellax-speed="2" src="<?php echo bloginfo('template_directory'); ?>/assets/img/img-background.png" />
    </div>
    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <div class="container">

            <!-- Conteúdo Valida se existe algum conteúdo -->
            <?php if (!empty(get_post()->post_content)) : ?>
                <div class="row">
                    <div class="col-12 col-md-6 content-info">
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    <?php endwhile; endif; ?>

</section>

<?php if(is_page('quem-somos')) : ?>
    <?php get_template_part('components/index/quem-somos'); ?>
<?php endif; ?>
<?php get_footer(); ?>