<!-- Valores institucionais -->
<?php $infos_secundarias = get_field('valores_institucionais');
$blocos = $infos_secundarias['valores_institucionais_blocos'];
if ($blocos) :
?>
    <div class="bloco-valor-rema py-5">
        <div class="container">
            <h2><span><?php echo $infos_secundarias['titulo_valores_institucionais']; ?> <?php echo $infos_secundarias['destaque_titulo_valor_institucional']; ?></span></h2>
            <p><?php echo $infos_secundarias['descricao_valores_institucionais']; ?></p>
            <div class="row justify-content-around text-center">

                <?php foreach ($blocos as $key => $bloco) : ?>
                    <div class="col-12 col-md-6 col-lg-4 bloco-valor my-4">
                        <div class="p-4 h-100 w-100">
                            <?php if($bloco['icone_valor_institucional']) : ?>
                                <figure>
                                    <img src="<?php echo wp_get_attachment_image_url( $bloco['icone_valor_institucional'], 'icone-valor' ); ?>" alt="<?php echo $bloco['titulo_valor_institucional']; ?>">
                                </figure>
                            <?php endif; ?>
                            <h3><span><?php echo $bloco['titulo_valor_institucional']; ?></span></h3>
                            <p><?php echo $bloco['descricao_valor_institucional']; ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<!-- Mídias -->
<?php
$midias_secundarias = get_field('midias');
$midias = $midias_secundarias['links_videos'];
if ($midias) :
?>
    <div class="bloco-valor-rema py-5">
        <div class="container">
            <div class="row justify-content-around text-center">
                <?php foreach($midias as $midia) : ?>
                    <h2 class="no-underline w-75 mb-2"><span><?php echo $midia['titulo_secao_midias']; ?> <?php echo $midia['destaque_titulo_midias']; ?></span></h2>
                
                    <div class="my-5">
                        <?php echo $midia['link_video']; ?>
                    </div>
                <?php endforeach; ?>
            </div>            
        </div>
    </div>
<?php endif; ?>