<div class="footer-copyright py-3 container">
    <div class="row">
        <div class="col-12">
            <div class="d-flex justify-content-between align-items-center">
                <h6><span><i class="far fa-copyright"></i> <?php echo date('Y'); ?> <?php echo bloginfo('name'); ?> - Todos os direitos reservados</span></h6>
                <a href="http://bit.ly/38DuDmd" target="_blank"> <img src="<?php bloginfo('template_directory'); ?>/assets/img/logo-cia.png" /></a>
            </div>
        </div>
    </div>
</div>