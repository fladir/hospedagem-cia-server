<?php
    // Banners que serão exibidos na sidebar do Single Post do Blog
    $args = array(
        'post_type' => 'page',
        'pagename'  => 'blog',
    );
    $WPQuery = new WP_Query($args);
?>
<?php if($WPQuery->have_posts()) : while($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
    <?php $banners = get_field('anuncio_banner_vertical'); ?>
        <?php foreach ($banners as $key => $banner) : ?>
            <!-- Verifica sem tem link -->
            <?php if ($banner['link_banner_vertical']) : ?>
                <a class="banner-sidebar" href="<?php echo $banner['link_banner_vertical']; ?>">
            <?php endif; ?>
            <!-- Imagem -->
            <img src="<?php echo wp_get_attachment_image_url($banner['imagem_banner_vertical'], 'post-banner'); ?>" alt="<?php echo get_the_title(); ?>">
            <!-- Fim tag link -->
            <?php if ($banner['link_banner_vertical']) : ?>
                </a>
            <?php endif; ?>
        <?php endforeach; ?>

<?php endwhile; endif; wp_reset_postdata(); ?>