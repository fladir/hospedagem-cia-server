<?php $rodape = get_field('grupo_footer', 'options'); ?>
    <?php $localizacao = get_nav_menu_locations(); ?>
    <?php $menu_obj = get_term($localizacao['secondary']); ?>
    <?php $nome_menu = $menu_obj->name; ?>
    <?php if(empty($rodape['titulo_coluna_2']) && empty($rodape['destaque_titulo_coluna_2'])) : ?>
        <h4><?php echo $nome_menu; ?></h4>
    <?php else: ?>
        <h2><span><?php echo $rodape['titulo_coluna_2']; ?></span> <?php echo $rodape['destaque_titulo_coluna_2']; ?></h2>
    <?php endif; ?>
    <?php $args = array(
        'theme_location' => 'secondary',
        'container'      => '<ul>',
        'depth'          => 1,
    );
    wp_nav_menu($args);
    ?>