<?php get_header(); ?>
<?php
// Get data from URL into variables
$_cidade = $_GET['cidade_emp'] != '' ? strip_tags(trim($_GET['cidade_emp'])) : '';
$_estagio = $_GET['tipo_emp'] != '' ? strip_tags(trim($_GET['tipo_emp'])) : '';
$args = array(
    //'s'     => get_search_query(),
    'post_type' => 'empreendimentos',
    'posts_per_page' => 10,
    'cat_ID'       => $_cidade,
    'orderby'    => 'date',
    'order'     => 'DESC',
    'paged'     => get_query_var('paged'),
    'meta_query' => array(
        array(
            'key' => 'condicao_construcao',
            'value' => $_estagio,
            'compare' => 'LIKE',
        ),
    ),
);

$WPQuery = new WP_Query($args);
?>
<!-- Header padrão -->
<?php get_template_part('components/header/header-empreendimentos'); ?>

<section class="main-blog">
    <div class="container">
        <div class="row">

            <div class="col-12 col-lg-8">
                <h2 class="title-single mb-5"><?php echo $WPQuery->found_posts >= 1 ? _e('Imóveis disponíveis') : _e('Resultado da busca'); ?></h2>
                <div class="row">

                <?php if ($WPQuery->have_posts()) : ?>
                    <?php while ($WPQuery->have_posts()) : ?>
                        <?php $WPQuery->the_post(); ?>
                        <div class="col-12 col-md-6 mb-5 post-blog wow fadeInLeft">
                            <a href="<?php the_permalink(); ?>">
                                <figure>
                                    <?php if (has_post_thumbnail()) : ?>
                                        <img src="<?php echo get_the_post_thumbnail_url($WPQuery->ID, 'blog-home'); ?>" alt="<?php echo get_the_title(); ?>">
                                    <?php else : ?>
                                        <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'blog-home'); ?>" alt="<?php echo get_the_title(); ?>">
                                    <?php endif; ?>
                                </figure>

                                <div class="texto-info">
                                    <?php
                                    $cat = wp_get_post_categories($WPQuery->ID);
                                    ?>
                                    <p class="category-blog"><?php echo $cat->category_name; ?></p>

                                    <h2 class="truncate3"><?php echo get_the_title(); ?></h2>

                                </div>
                            </a>
                        </div>
                    <?php endwhile; ?>
                 <?php else: ?>
                    <h3>Não foi encontrado imóvel.</h3>
                <?php endif; ?>
                </div>
                <?php require get_template_directory() . '/core/components/paginacao/paginacao.php'; ?>
            </div>
            <!-- Sidebar -->
            <aside class="col-12 col-lg-3 offset-lg-1 wow fadeInRight banners-sidebar">
                <!-- Banners da lateral -->
                <?php get_template_part('components/blog/banners-single'); ?>
            </aside>
        </div>
    </div>
</section>

<?php wp_reset_postdata(); ?>

<?php get_footer(); ?>