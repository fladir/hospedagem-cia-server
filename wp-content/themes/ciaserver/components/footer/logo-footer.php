<?php
    $logo = get_theme_mod('custom_logo');
   $logo_alt = get_theme_mod('alter_logo');

   $footer_type = get_theme_mod('footer_logo');
?>
<?php $redes = get_field('grupo_informacoes_para_contato', 'options')['redes_sociais']; ?>

<a href="<?php echo bloginfo('url'); ?>">
    <figure>
        <?php if($footer_type === "logo") : ?>
            <img src="<?php echo wp_get_attachment_image_url($logo, 'thumb-footer'); ?>" alt="<?php echo get_bloginfo('name'); ?>">
        <?php elseif($footer_type === "logo-alt"): ?>
            <img src="<?php echo wp_get_attachment_image_url($logo_alt, 'thumb-footer'); ?>" alt="<?php echo get_bloginfo('name'); ?>">
        <?php endif; ?>
    </figure>
</a>

<?php if($redes) : ?>
    <div class="d-flex justify-content-center icon-footer mt-4">
        <?php foreach ($redes as $key => $rede) : ?>
        <a href="<?php echo $rede['link_social']; ?>">
            <i class="<?php echo $rede['icone_social']; ?>"></i>
        </a>
        <?php endforeach; ?>
    </div>
<?php endif; ?>