<?php $catalogos = get_field('catalogo_produto');
if ($catalogos) :
?>
    <div class="container">
        <?php foreach ($catalogos as $key => $catalogo) : ?>
        <?php if($catalogo['arquivo_pdf']) : ?>
            <a href="<?php echo $catalogo['arquivo_pdf']; ?>" download>
                <div class="col-12 my-5 ">
                    <div class="row align-items-center download-section">
                        <div class="col-3 col-md-1 pl-4">
                            <figure>
                                <img src="<?php echo bloginfo('template_directory'); ?>/assets/img/download_icone.png" alt="<?php echo $catalogo['titulo_catalogo']; ?>">
                            </figure>
                        </div>
                        <div class="col-9 col-md-11 pr-4">
                            <h3><?php echo $catalogo['titulo_catalogo']; ?></h3>
                            <p><?php echo $catalogo['descricao_catalogo']; ?></p>
                        </div>
                    </div>
                </div>
            </a>
        <?php #else: ?>
            <!-- <p>Não tem catálogos para esse produto</p> -->
        <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>