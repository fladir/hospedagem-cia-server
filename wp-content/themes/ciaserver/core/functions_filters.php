<?php

//function pbo_custom_wp_title($title, $sep)
//{
//
//    // Adicionando o nome do site
//    $title = getblogin('grupo_header', 'options')['nome_da_empresa'] ? get_field('grupo_header', 'options')['nome_da_empresa'] : get_bloginfo('name');
//    $slogan = get_field('grupo_header', 'options')['slogan'] ? get_field('grupo_header', 'options')['slogan'] : get_bloginfo('description');
//
//    if (is_feed()) {
//        return $title;
//    }
//
//    global $paged, $page;
//
//    // Adicione a descrição do site para a página inicial / principal.
//    if(is_home() || is_front_page()){
//        $title .= " $sep " . $slogan;
//    } elseif(is_search()) {
//        $title .= " $sep " . 'Resultado da busca';
//    } elseif (is_tax()) {
//        $title .= " $sep ". get_queried_object()->name;
//    } elseif (is_404()) {
//        $title .= " $sep " . 'Página não encontrada';
//    } else {
//        $title .= " $sep " . get_the_title();
//    }
//
//    // Adicione um número de página, se necessário.
//    if ($paged >= 2 || $page >= 2) {
//        $title .= " $sep " . sprintf(__('Página %s', '_s'), max($paged, $page));
//    }
//
//    return $title;
//}
//add_filter('wp_title', 'pbo_custom_wp_title', 10, 2);

add_filter('show_admin_bar', '__return_false');

# Adicionando a coluna de post vistos no admin de posts
// function chr_posts_columns_views($defaults){
//    $defaults['post_views'] = __('Visualização(ões)');
//    return $defaults;
// }
// add_filter('manage_posts_columns', 'chr_posts_columns_views');