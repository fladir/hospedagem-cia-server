<?php get_header() ?>

<!-- Slide -->
<?php get_template_part('components/slide-bootstrap/slide'); ?>

<!-- Lançamento -->
<?php get_template_part('components/index/lancamento'); ?>

<!-- Empreendimentos -->
<?php get_template_part('components/index/empreendimento'); ?>

<!-- Call to action -->
<?php get_template_part('components/index/call-to-action'); ?>

<!-- Depoimentos -->
<?php get_template_part('components/index/depoimentos'); ?>

<!-- Quem somos -->
<?php get_template_part('components/index/quem-somos'); ?>

<!-- Mapa -->
<?php get_template_part('components/index/mapa'); ?>

<?php get_footer() ?>