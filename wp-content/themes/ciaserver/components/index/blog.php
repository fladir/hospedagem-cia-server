<?php
    $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
?>
<section class="blog-home">

        <div class="container">
            <h2 class="title-home">Nosso Blog</h2>
            <div class="row justify-content-center mt-65-menos">
                <?php
                $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
                $args = array(
                    'post_type' => 'post',
                    'orderby'   => 'date',
                    'order'     => 'DESC',
                    'posts_per_page' => 3,
                );
                $WPQuery = new WP_Query($args);
                ?>
                <?php if ($WPQuery->have_posts()) : ?>
                    <?php while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                        <div class="col-12 col-md-4 wow fadeIn my-5 my-md-0 post-blog">
                            <a href="<?php the_permalink(); ?>">

                                <figure>
                                <?php if (has_post_thumbnail()) : ?>
                                    <img src="<?php echo get_the_post_thumbnail_url($WPQuery->ID, 'blog-home'); ?>" alt="<?php echo get_the_title(); ?>">
                                <?php else : ?>
                                    <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'blog-home'); ?>" alt="<?php echo get_the_title(); ?>">
                                <?php endif; ?>
                                </figure>

                                <div class="texto-info">
                                    <?php $category = get_the_category()[0]; ?>
                                    <p class="category-blog"> <?php echo $category->name; ?></p>
                                    <h3 class="truncate3"><?php echo get_the_title(); ?></h3>
                                </div>

                            </a>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            <?php wp_reset_postdata(); ?>

                <div class="mt-40">
                    <a href="<?php echo bloginfo('url'); ?>/blog" class="btn btn-default hvr-icon-wobble-horizontal">Ver todos <img class="hvr-icon" src="<?php echo bloginfo('template_directory'); ?>/assets/img/seta-default.png" alt="Saiba mais"></a>
                </div>
            </div>
        </div>
</section>