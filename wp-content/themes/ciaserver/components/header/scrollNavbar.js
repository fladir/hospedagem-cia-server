window.onscroll = () => {
    let navbar = document.querySelector('#menu-secundario');
    let logo = document.querySelector('.navbar-brand');
    let menuIcon2 = document.querySelector('.menuIcon2');
    let menuIcon3 = document.querySelector('.menuIcon3');

    if(window.scrollY > 100 ){
        navbar.classList.add("fixed-top");
        navbar.classList.remove('fadeIn');
        navbar.classList.add("slideInDown");
        logo.classList.add('logo-main');
        navbar.classList.add('bg-menu');
        navbar.classList.remove('menu-absolute');
        navbar.classList.add("shadow-menu");
        menuIcon2.classList.add('top-mobile');
        menuIcon3.classList.add('top-mobile');
    } else {
        navbar.classList.remove("fixed-top");
        navbar.classList.remove("slideInDown");
        navbar.classList.remove('bg-menu');
        navbar.classList.add('menu-absolute');
        logo.classList.remove('logo-main');
        navbar.classList.remove("shadow-menu");
        menuIcon2.classList.remove('top-mobile');
        menuIcon3.classList.remove('top-mobile');
    }
}