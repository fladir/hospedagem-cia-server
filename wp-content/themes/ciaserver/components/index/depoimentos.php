<?php
$args = array(
    'post_type' => 'depoimentos',
    'posts_per_page' => -1,
);
$WPQuery = new WP_Query($args);
?>
<?php if($WPQuery->found_posts >= 1) : ?>

    <section class="depoimentos-home">
        <!--  Imagens de background -->
        <img class="bg-section topo" src="<?php echo bloginfo('template_directory'); ?>/assets/img/aspas-topo.png" >
        <img class="bg-section bottom" src="<?php echo bloginfo('template_directory'); ?>/assets/img/aspas-bottom.png" >

        <!-- Conteúdo -->
        <div class="container">
            <!-- Título -->
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center">O que nossos clientes dizem</h2>
                </div>
            </div>
            <!-- Depoimento -->
            <div class="row">
                <div class="col-12 col-md-8 offset-md-2">
                    <!-- Carousel -->
                    <div id="depoimentos-box" class="owl-carousel owl-theme">

                        <?php if($WPQuery->have_posts()) : while($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                            <!-- Depoimento -->
                            <div class="depoimentos">
                                <!-- Borda superior -->
                                <img src="<?php echo bloginfo('template_directory'); ?>/assets/img/depoimento-box-top.png">

                                <!-- Texto -->
                                <div class="col-10 offset-1 box-texto">
                                    <!-- Declaração -->
                                    <?php the_content(); ?>

                                    <!-- Nome e data de compra -->
                                    <div class="text-right my-5">
                                        <h3><?php the_title(); ?></h3>

                                        <?php $compra = get_field('compra_imovel'); ?>
                                        <?php if($compra) : ?>
                                            <span><?php echo $compra; ?></span>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <!-- Borda inferior -->
                                <img class="img-bottom" src="<?php echo bloginfo('template_directory'); ?>/assets/img/depoimento-box-bottom.png">
                            </div>

                        <?php endwhile; endif; wp_reset_postdata(); ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>
