<?php $slides = get_field('slide_repetidor'); ?>
<section class="slides">
    <?php if ($slides) : ?>
        <div id="slide-bootstrap" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators justify-content-center">

                <?php foreach ($slides as $key => $slide) : ?>

                    <li data-target="#slide-bootstrap" data-slide-to="<?php echo $key; ?>" class="<?php echo $key == 0 ? "active" : "" ?>"></li>

                <?php endforeach; ?>

            </ol>


            <div class="carousel-inner">


                <?php foreach ($slides as $key => $slide) : ?>
                    <div class="carousel-item <?php echo $key == 0 ? 'active' : '' ?>">
                        <!-- Lente branca -->
                        <div class="box-white"></div>

                        <!-- Lente escura -->
                        <div class="box-dark"></div>


                    <?php if($slide['link_slide_imagem']) : ?>
                        <a href="<?php echo $slide['link_slide_imagem']; ?>">
                    <?php endif; ?>
                        <!-- Se for mobile e possuir Banner Mobile -->
                        <?php if(wp_is_mobile() && $slide['imagem_mobile']) : ?>
                            <img class="d-block w-100" src="<?php echo wp_get_attachment_image_url($slide['imagem_mobile'], 'slides-mobile'); ?>" alt="<?php echo get_the_title(); ?>" >
                        <?php else: ?>
                            <img class="d-block w-100" src="<?php echo wp_get_attachment_image_url($slide['imagem'], 'slides'); ?>" alt="<?php echo get_the_title(); ?>" >
                        <?php endif; ?>


                        <?php if( $slide['titulo'] || $slide['sub_titulo'] ) : ?>
                        <div class="carousel-caption col-12 col-md-6 col-xl-3">

                            <div class="bg-slide rellax" data-rellax-speed="1">
                                <h2 class="rellax" data-rellax-speed="4"><?php echo $slide['titulo'] ?></h2>
                                <p class="rellax" data-rellax-speed="3"><?php echo $slide['sub_titulo']; ?></p>
                                <?php if ($slide['link_botao'] && $slide['texto_botao']) : ?>
                                    <a href="<?php echo $slide['link_botao']; ?>" class="btn btn-secundario rellax hvr-icon-wobble-horizontal" data-rellax-speed="2" target="_blank">
                                        <?php echo $slide['texto_botao']; ?>
                                        <i class="hvr-icon fas fa-angle-double-right"></i>
                                    </a>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php endif; ?>
                    <?php if($slide['link_slide_imagem']) : ?>
                        </a>
                    <?php endif; ?>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    <?php endif; ?>
</section>
