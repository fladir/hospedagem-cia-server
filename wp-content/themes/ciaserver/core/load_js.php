<?php

function pbo_load_js()
{
    # wp_enqueue_script('loadingJS', get_template_directory_uri() . '/core/components/loading/loading.js', 1, false);


    # Custom JS
    wp_enqueue_script('waypoint-Js', get_template_directory_uri() . '/assets/libs/waypoints/jquery.waypoints.min.js', array('jquery'), 1, true);
    wp_enqueue_script('jquery-easing-Js', get_template_directory_uri() . '/assets/libs/jquery-easing/jquery.easing.min.js', array('jquery'), 1, true);
    wp_enqueue_script('owlCarousel-Js', get_template_directory_uri() . '/assets/libs/owl-carousel/owl.carousel.min.js', array('jquery'), 1, true);
    #wp_enqueue_script('Popper-Js', get_template_directory_uri() . '/assets/libs/popper-js/popper.min.js', array('jquery'), 1, false);
    wp_enqueue_script('WOW-Js', get_template_directory_uri() . '/assets/libs/wow-js/wow.min.js', array('jquery'), 1, true);
    wp_enqueue_script('Bootstrap-js', get_template_directory_uri() . '/assets/libs/bootstrap/js/bootstrap.min.js', array('jquery'), 1, true);
    wp_enqueue_script('FontAwesome', get_template_directory_uri() . '/assets/libs/fontawesome-free/js/all.min.js', array('jquery'), 1, true);
    wp_enqueue_script('fancyBox-Js', get_template_directory_uri() . '/assets/libs/fancybox/jquery.fancybox.min.js', array('jquery'), 1, true);
    wp_enqueue_script('rellax-Js', get_template_directory_uri() . '/assets/libs/rellax-js/rellax.min.js', array('jquery'), 1, true);
    wp_enqueue_script( 'Seta-JS', get_template_directory_uri() . '/core/components/setaParaTopo/seta.js', array('jquery'), 1, true );
    wp_enqueue_script('utilsCore', get_template_directory_uri() . '/core/assets/js/utilsFramework.js', array('jquery'), 1, true);
    wp_enqueue_script('Global-Js', get_template_directory_uri() . '/assets/js/global.js', array('jquery'), 1, true);



}

add_action('wp_enqueue_scripts', 'pbo_load_js');
