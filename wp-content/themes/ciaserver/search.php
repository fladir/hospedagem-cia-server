<?php get_header(); ?>

<?php
$args = array(
    's'     => get_search_query(),
    'post_type' => array('post'),
    'posts_per_page' => 10,
    'orderby'    => 'date',
    'order'     => 'DESC',
    'paged'     => get_query_var('paged'),
);

$WPQuery = new WP_Query($args);
?>
<!-- Header padrão -->
<?php get_template_part('components/header/header-padrao'); ?>

<section class="main-blog">
    <div class="container">
        <div class="row">

            <div class="col-12 col-lg-8">
                <h2 class="title-single mb-5"><?php _e('Resultados para o(s) termo(s): '); ?> "<?php the_search_query(); ?>"</h2>
                <div class="row">
                <?php if ($WPQuery->have_posts()) : ?>
                    <?php while ($WPQuery->have_posts()) : ?>
                        <?php $WPQuery->the_post(); ?>

                        <div class="col-12 col-md-6 mb-5 post-blog wow fadeInLeft">
                            <a href="<?php the_permalink(); ?>">
                                <figure>
                                    <?php if (has_post_thumbnail()) : ?>
                                        <img src="<?php echo get_the_post_thumbnail_url($WPQuery->ID, 'blog-home'); ?>" alt="<?php echo get_the_title(); ?>">
                                    <?php else : ?>
                                        <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'blog-home'); ?>" alt="<?php echo get_the_title(); ?>">
                                    <?php endif; ?>
                                </figure>

                                <div class="texto-info">
                                    <p class="category-blog"><?php echo get_the_date('M Y'); ?></p>

                                    <h2 class="truncate3"><?php echo get_the_title(); ?></h2>

                                </div>
                            </a>
                        </div>


                    <?php endwhile; ?>
                <?php endif; ?>
                </div>
                <?php require get_template_directory() . '/core/components/paginacao/paginacao.php'; ?>
            </div>
            <!-- Sidebar -->
            <aside class="col-12 col-lg-3 offset-lg-1">
                <!-- Pesquisar e categoria -->
                <?php dynamic_sidebar( 'sidebar-right' ); ?>

                <!-- Mais lidos e banners -->
                <?php get_template_part( '/components/blog/sidebar' ); ?>
            </aside>
        </div>
    </div>
</section>

<?php wp_reset_postdata(); ?>

<?php get_footer(); ?>