jQuery(document).ready(function () {
    //Botão Menu Mobile
    jQuery('.nav-toggler, .mobile-menu-overlay').click(function () {
      jQuery('.nav-toggler').toggleClass('toggler-open');
      jQuery('.mobile-menu-wrapper, .mobile-menu-overlay').toggleClass('open');
      jQuery('body, #menu-secundario').toggleClass('arredar');
    });
    jQuery().fancybox({
    selector: '.wp-block-gallery > .blocks-gallery-grid > .blocks-gallery-item > figure > a:visible',
    })
    jQuery().fancybox({
    selector: '.gallery > .gallery-item > .gallery-icon > a:visible',
    })
});

document.addEventListener("DOMContentLoaded", function (event) {
    instanciarCarousel('#empreendimentos');
    instanciarPostsSlide('#depoimentos-box');
    instanciarPortfolioSlide('#galeria-solucao');
    instanciarPostsSlide('#banners-relacionados');
    instanciarCarouselSingleEmpreendimento('#galeria-slide');
});

new Rellax('.rellax');
new WOW().init();


// Animação dos itens do Status da obra - Single Empreedimento
(function ($) {
    $(document).ready(function () {
        $('.status-info').waypoint(function () {
            $('.count').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 5000,
                    easing: 'easeOutExpo',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
            $('.numeros').addClass('animated fadeIn faster');
            this.destroy()
        }, {offset: '80%'});
    });

    $('.circle-wrap').waypoint(function () {
        $('.trigger').addClass('animate');
    }, {offset: '80%'});
})(jQuery);

async function instanciarCarousel(seletor) {
    jQuery(seletor).owlCarousel({
        loop: true,
        margin: 30,
        autoplay: true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        nav: false,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        dots: false,
        responsive: {
            0: {
                items: 1,

            },
            768: {
                items: 3,
            }
        }
    })
}

async function instanciarPortfolioSlide(seletor){
    jQuery(seletor).owlCarousel({
        loop: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: true,
        nav: false,
        dots: true,
        dotData: false,
        dotsData: false,
        items: 1,
    })
}

async function instanciarPostsSlide(seletor){
    jQuery(seletor).owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        nav: false,
        dots: false,
        items: 1,
    })
}

// Single Empreendimento
async function instanciarCarouselSingleEmpreendimento(seletor) {
    jQuery(seletor).owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        nav: true,
        navText: ['<img src="http://ciaserver/s/silveiracorrea.com.br/public/wp-content/themes/silveira/assets/img/anterior-slide.png" />', '<img src="http://ciaserver/s/silveiracorrea.com.br/public/wp-content/themes/silveira/assets/img/proximo-slide.png" />'],
        dots: false,
        items: 1,
        responsive: {
            0: {
                autoplay: false,
                autoplayTimeout: 240000,
            },
            768: {
                autoplay: true,
            }
        }
    })
}

async function instanciarDiferencial(seletor) {
    jQuery(seletor).owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        nav: true,
        navText: ['<img src="http://ciaserver/a/alternova.com.br/public/wp-content/themes/alternova/assets/img/dif-anterior.png" />', '<img src="http://ciaserver/a/alternova.com.br/public/wp-content/themes/alternova/assets/img/dif-proximo.png" />'],
        dots: false,
        responsive: {
            0: {
                items: 1,

            },
            768: {
                items: 3,
            },
            1200: {
                items: 5,
            }
        }
    })
}

async function instanciarPlantasSlide(seletor){
    jQuery(seletor).owlCarousel({
        loop: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: false,
        nav: false,
        dots: true,
        dotData: true,
        dotsData: true,
        items: 1,
    })
}

// Maps
(function( $ ) {

    /**
     * initMap
     *
     * Renders a Google Map onto the selected jQuery element
     *
     * @date    22/10/19
     * @since   5.8.6
     *
     * @param   jQuery $el The jQuery element.
     * @return  object The map instance.
     */
    function initMap( $el ) {

        // Find marker elements within map.
        var $markers = $el.find('.marker');

        // Create gerenic map.
        var mapArgs = {
            zoom        : $el.data('zoom') || 16,
            mapTypeId   : google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map( $el[0], mapArgs );

        // Add markers.
        map.markers = [];
        $markers.each(function(){
            initMarker( $(this), map );
        });

        // Center map based on markers.
        centerMap( map );

        // Return map instance.
        return map;
    }

    /**
     * initMarker
     *
     * Creates a marker for the given jQuery element and map.
     *
     * @date    22/10/19
     * @since   5.8.6
     *
     * @param   jQuery $el The jQuery element.
     * @param   object The map instance.
     * @return  object The marker instance.
     */
    function initMarker( $marker, map ) {

        // Get position from marker.
        var lat = $marker.data('lat');
        var lng = $marker.data('lng');
        var latLng = {
            lat: parseFloat( lat ),
            lng: parseFloat( lng )
        };

        // Create marker instance.
        var marker = new google.maps.Marker({
            position : latLng,
            map: map
        });

        // Append to reference for later use.
        map.markers.push( marker );

        // If marker contains HTML, add it to an infoWindow.
        if( $marker.html() ){

            // Create info window.
            var infowindow = new google.maps.InfoWindow({
                content: $marker.html()
            });

            // Show info window when marker is clicked.
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open( map, marker );
            });
        }
    }

    /**
     * centerMap
     *
     * Centers the map showing all markers in view.
     *
     * @date    22/10/19
     * @since   5.8.6
     *
     * @param   object The map instance.
     * @return  void
     */
    function centerMap( map ) {

        // Create map boundaries from all map markers.
        var bounds = new google.maps.LatLngBounds();
        map.markers.forEach(function( marker ){
            bounds.extend({
                lat: marker.position.lat(),
                lng: marker.position.lng()
            });
        });

        // Case: Single marker.
        if( map.markers.length == 1 ){
            map.setCenter( bounds.getCenter() );

            // Case: Multiple markers.
        } else{
            map.fitBounds( bounds );
        }
    }

// Render maps on page load.
    $(document).ready(function(){
        $('.acf-map').each(function(){
            var map = initMap( $(this) );
        });
    });

})(jQuery);