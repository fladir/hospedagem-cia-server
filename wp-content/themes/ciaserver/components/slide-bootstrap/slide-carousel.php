<?php $slides = get_field('slide_repetidor'); ?>
<section class="slides">
    <div class="owl-carousel owl-theme" id="slide-home">
        <?php foreach ($slides as $key => $slide) : ?>
            <div class="item wow fadeInRightBig">
                <?php if($slide['link_slide_imagem']) : ?>
                <a href="<?php echo $slide['link_slide_imagem']; ?>">
                    <?php endif; ?>
                    <figure>
                        <!-- Se for mobile e possuir Banner Mobile -->
                        <?php if(wp_is_mobile() && $slide['imagem_mobile']) : ?>
                            <img class="d-block w-100" src="<?php echo wp_get_attachment_image_url($slide['imagem_mobile'], 'slides-mobile'); ?>" alt="<?php echo get_the_title(); ?>" >
                        <?php else: ?>
                            <img class="d-block w-100" src="<?php echo wp_get_attachment_image_url($slide['imagem'], 'slides'); ?>" alt="<?php echo get_the_title(); ?>" >
                        <?php endif; ?>
                    </figure>
                    <div class="title">
                        <h2 class="rellax" data-rellax-speed="5"><?php echo $slide['titulo'] ?></h2>
                        <p class="rellax" data-rellax-speed="3"><?php echo $slide['sub_titulo']; ?></p>
                        <?php if ($slide['link_botao'] && $slide['texto_botao']) : ?>
                            <a href="<?php echo $slide['link_botao']; ?>" class="btn btn-secundario rellax" data-rellax-speed="1" target="_blank">
                                <?php echo $slide['texto_botao']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                    <?php if($slide['link_slide_imagem']) : ?>
                </a>
            <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</section>
<?php wp_enqueue_script('slideJS', get_template_directory_uri() . '/components/slide-bootstrap/slide-carousel.js', array('jquery'), 1, true);  ?>