<?php
/* Template Name: Contato */
get_header();
?>
<!-- Header padrão -->
<?php get_template_part('components/header/header-padrao'); ?>
<!-- Fim header padrão -->
<section class="contato-page">

    <!--  Box Verde  -->
    <div class="background-wrap">
        <div class="background"></div>
    </div>
    <!--  Imagem que sobrepõe parte do slider  -->
    <div class="img-bg">
        <img class="rellax" data-rellax-speed="2" src="<?php echo bloginfo('template_directory'); ?>/assets/img/img-background.png" />
    </div>

    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <?php the_content(); ?>
            </div>
        </div>

        <div class="row">
            <!-- Formulário -->
            <div class="col-12 col-md-6 content-info">
                <?php echo do_shortcode('[contact-form-7 id="5" title="Contato"]'); ?>
            </div>
            <!-- Telefones e email -->
            <div class="col-12 col-md-5 offset-md-1 contato-info">
                <h2>Contato</h2>
                <?php
                $info_topo = get_field('grupo_informacoes_para_contato', 'options');
                $whatsapp = $info_topo['whatsapp'];
                $emails = $info_topo['emails'];
                $telefones = $info_topo['telefones'];
                $enderecos = $info_topo['enderecos'];
                ?>

                <?php if($telefones || $emails || $whatsapp || $enderecos) : ?>
                    <ul>
                        <?php if($enderecos) : ?>
                            <?php foreach ($enderecos as $key => $end) : ?>
                                <li>
                                    <span><?php echo $end['endereco']; ?></span>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <?php if($emails) : ?>
                            <?php foreach($emails as $key => $mail) : ?>
                                <li class="mail">
                                    <a href="mailto:<?php echo $mail['endereco_email']; ?>" target="_blank">
                                        <?php echo $mail['endereco_email']; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <?php if($whatsapp) : ?>
                            <?php foreach($whatsapp as $key => $whats) : ?>
                                <li class="phone">
                                    <a href="https://api.whatsapp.com/send?phone=55<?php echo $whats['link_whatsapp']; ?>" target="_blank">
                                        <?php echo $whats['numero_whatsapp']; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <?php if($telefones) : ?>
                            <?php foreach($telefones as $key => $fone) : ?>
                                <li class="phone">
                                    <a href="tel:<?php echo $fone['numero_telefone']; ?>" target="_blank">
                                        <?php echo $fone['numero_telefone']; ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </ul>
                <?php endif; ?>
            </div> <!-- contato-info -->

            
        </div> <!-- row -->
    </div> <!-- container -->

    <?php endwhile; 
        endif;
        wp_reset_postdata();
    ?>
</section>
<?php get_footer(); ?>