
<div class="row">

    <div class="col-12">
        <form class="form-inline form-blog" id="searchform" method="get" action="<?php echo home_url('/'); ?>">
            <div class="form-group px-0 formBlog">
                <div class="form-group input-group">
                    <input name="s" class="form-control" type="text" value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>" id="pesquisa" placeholder="Faça uma busca" />
                    <input type="hidden" name="post_type" value="post" />
                    <button type="submit" class="pl-0 btn"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>

</div>