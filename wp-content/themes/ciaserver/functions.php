<?php

add_filter('show_admin_bar', '__return_false');

# CIA SETTINGS

Add_filter('show_admin_bar', '__return_false');

$themename = "Configurações do Site";
define('THEME_NAME', $themename);
define('THEME_FOLDER', "ciawebsites");
define('THEME_VER', 5);

# PBO SETTINGS
require_once get_template_directory() . '/core/index.php';

// Chave da API do Maps
function my_acf_init()
{
    acf_update_setting('google_api_key', 'AIzaSyCxQHlF-7oXndl6laoEGZCzQ9kho7HhJzQ');
}

add_action('acf/init', 'my_acf_init');


//Form de busca personalizado
function wpse_load_custom_search_template(){
    if( isset($_REQUEST['search']) == 'empreendimentos' ) {
        require('search-empreendimentos.php');
        die();
    }
}
add_action('init','wpse_load_custom_search_template');


// Contact Form apontando para a página de sucesso
add_action('wp_footer', 'form_send_page');
function form_send_page(){
?>
    <script type="text/javascript">
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            if ( '206' == event.detail.contactFormId ) {
                window.location.href="<?php echo bloginfo('url'); ?>/mensagem-enviada";
            }
        }, false );
    </script>
<?php
}
?>