<?php

//Menu lateral Configurações do tema
if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array(
    'page_title'   => 'Configurações',
    'menu_title'  => 'Configurações do Tema',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'icon_url'      => get_template_directory_uri() . '/core/assets/img/favicon.png',
    'position'      => '1000',
    'redirect'    => false
  ));
}

function pbo_settings_theme()
{

  register_nav_menus(array(
    'primary' => __('Cabeçalho', 'CiaWebsites'),
    'secondary' => __('Footer', 'CiaWebsites'),
  ));
}
add_action('init', 'pbo_settings_theme');

include get_template_directory() . '/core/functions/pagination-bootstrap.php';

function ajax()
{

  // Define a variável ajaxurl
  $script  = '<script>';
  $script .= 'const ajaxurl = "' . admin_url('admin-ajax.php') . '";';
  $script .= '</script>';
  echo $script;
}

function custom_setup() {
    // Título tag
    add_theme_support( 'title-tag' );
    // Logo
    add_theme_support('custom-logo');
    // Thumbnails
    add_theme_support('post-thumbnails');
}
add_action( 'after_setup_theme', 'custom_setup' );

// Post format
$post_formats = array('gallery');
add_theme_support('post-formats', $post_formats);
add_theme_support('html5', array('gallery'));
add_post_type_support('page', 'excerpt');

// Widgets
add_theme_support( 'customize-selective-refresh-widgets' );

// Adiciona no rodapé
add_action('wp_footer', 'ajax');

// Contagem dos posts para retornar os mais lidos no Blog index
function chr_setPostViews($postID)
{
  $countKey = 'post_views_count';
  $count = get_post_meta($postID, $countKey, true);

  if ($count == '') {
    $count = 0;
    delete_post_meta($postID, $countKey);
    add_post_meta($postID, $countKey, '0');
    return 'Nenhuma Visualização';
  } elseif (is_single($postID)) {
    $count++;
    update_post_meta($postID, $countKey, $count);
  }

  return $count . ' Visualização(ões)';
}

# Funcoes do PBO Framework

require_once(get_template_directory() . '/functions/empreendimentos.php');
require_once(get_template_directory() . '/functions/depoimentos.php');