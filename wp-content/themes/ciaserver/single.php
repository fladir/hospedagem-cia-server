<?php get_header();
chr_setPostViews(get_the_ID()); #contador de views
?>
<!-- Header padrão -->
<?php get_template_part('components/header/header-single-blog'); ?>
<?php if (have_posts()) :

    while (have_posts()) : the_post(); ?>
        <!-- Post -->
        <section class="single-blog wow fadeIn">
            <?php $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao']; ?>

            <article>
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-8">
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-lg-8 info-single">
                            <?php if (has_post_thumbnail()) : ?>
                                <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'blog-destaque'); ?>" alt="<?php echo get_the_title(); ?>">
                            <?php else: ?>
                                <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'blog-destaque'); ?>" alt="<?php echo get_the_title(); ?>">
                            <?php endif; ?>

                            <?php the_content(); ?>

                            <!-- Compartilhar -->
                            <div class="mt-5">
                                <?php echo do_shortcode('[ssba-buttons]'); ?>
                            </div>

                        </div>

                        <!-- Sidebar -->
                        <aside class="col-12 col-lg-3 offset-lg-1 wow fadeInRight banners-sidebar">
                            <!-- Banners da lateral -->
                            <?php get_template_part('components/blog/banners-single'); ?>
                        </aside>
                        
                    </div>
                    <!-- Posts relacionados -->
                    <?php get_template_part('components/blog/posts-relacionados'); ?>
                </div>
            </article>
        </section>
<?php endwhile;
endif; ?>
<?php get_footer(); ?>