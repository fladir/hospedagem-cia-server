<?php
// Custom image sizes
add_image_size('logo', 390, 0);
add_image_size('slides', 1920, 960); // Slide do topo - HOME
add_image_size('slides-mobile', 768, 0); // Slide do topo - HOME

// Home
add_image_size('destaque-lancamento', 550, 650, array('center', 'center')); // Imagem destaque - Lançamento
add_image_size('logo-empreendimento', 400, 0); // Logo do empreendimento - Lançamento
add_image_size('icone-lista', 40, 40); // Ícones da lista - Lançamento
add_image_size('empreendimento-home',360,400, array('center', 'center')); // Empreendimentos lista - Mais Empreendimentos
add_image_size('call-to-action', 1920, 0); // Call to action
add_image_size('icone-institucional', 280, 280); // Por que a Alternova
add_image_size('contato-form', 1920, 0); // Imagem de fundo do Formulário
add_image_size('blog-home', 350, 440, array('center', 'center')); // Blog index


// Páginas Internas
add_image_size('header-padrao', 1920, 620, array('center', 'center')); // Header padrão

// Blog
add_image_size('posts-recentes-blog', 83, 56, array('center', 'center')); // Mais lidos
add_image_size('blog-destaque', 760, 0); // Single destaque blog
add_image_size('post-relacionado', 360, 240, array('center', 'center')); // Blog index
add_image_size('post-banner', 260, 0); // Banners laterais - Single Blog

// Single empreendimento
add_image_size('galeria-empreendimento', 940, 635, array('center', 'center')); // Seção Galeria
add_image_size('icone-empreendimento', 45, 45); // Ícone diferencial
add_image_size('banner-tour', 1920, 790, array('center', 'center')); // Banner tour virtual
add_image_size('thumb-planta', 0, 650); // Plantas do imóvel