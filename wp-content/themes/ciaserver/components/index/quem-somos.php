<?php
    $args = array(
        'post_type' => 'page',
        'pagename'  => 'quem-somos',
    );
    $WPQuery = new WP_Query($args);

?>
<section class="quemsomos-section">
    <?php if($WPQuery->have_posts()) : while($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
<!--        Imagem de fundo -->
    <?php $img_bg = get_field('background_fundo'); ?>
    <?php if($img_bg) : ?>
       <img class="img-bg" src="<?php echo wp_get_attachment_image_url($img_bg, 'full'); ?>" alt="<?php echo get_field('titulo_secao'); ?>" >
    <?php endif; ?>
    <div class="container">
        <h2><?php echo get_field('titulo_secao'); ?></h2>
        <div class="row">
            <?php $itens = get_field('argumentos'); ?>
            <?php foreach ($itens as $key => $item) : ?>
            <div class="col-12 col-md-6 col-lg-4 p-lg-0 box-mobile">
                <div class="quemsomos-box">
                    <img src="<?php echo wp_get_attachment_image_url($item['icone_fundo'], 'icone-institucional'); ?>" alt="<?php echo $item['titulo_argumentos']; ?>">
                    <h3><?php echo $item['titulo_argumentos']; ?></h3>
                    <p><?php echo $item['argumento']; ?></p>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php endwhile; endif; wp_reset_postdata(); ?>
</section>