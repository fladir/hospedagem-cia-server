<?php
$term = get_queried_object();
$img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
if(is_category()){
    $args = array(
        'post_type' => 'post',
        'paged'     => $paged,
        'posts_per_page' => 10,
        'order'     => 'DESC',
        'cat'   => $term->term_id,
    );
}else{
    $args = array(
        'post_type' => 'post',
        'paged'     => $paged,
        'posts_per_page' => 10,
        'orderby'   => 'date',
        'order'    => 'DESC'
    );
}
$WPQuery = new WP_Query($args);
?>
<div class="row">
    <?php if ($WPQuery->have_posts()) : ?>
        <?php while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
            <div class="col-12 col-md-6 mb-5 post-blog wow fadeInLeft">
                <a href="<?php the_permalink(); ?>">
                    <figure>
                        <?php if (has_post_thumbnail()) : ?>
                            <img src="<?php echo get_the_post_thumbnail_url($WPQuery->ID, 'blog-home'); ?>" alt="<?php echo get_the_title(); ?>">
                        <?php else : ?>
                            <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'blog-home'); ?>" alt="<?php echo get_the_title(); ?>">
                        <?php endif; ?>
                    </figure>

                <div class="texto-info">
                    <p class="category-blog"><?php echo get_the_date('M Y'); ?></p>

                    <h2 class="truncate3"><?php echo get_the_title(); ?></h2>

                </div>
                </a>
            </div>

        <?php endwhile; ?>
    <?php endif; ?>
    <?php require get_template_directory() . '/core/components/paginacao/paginacao.php'; ?>
</div>
<?php /* Restore original Post Data */ wp_reset_postdata(); ?>