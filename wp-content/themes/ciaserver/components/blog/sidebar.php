<div class="sidebar col-12 py-3 mt-4">
    <h3>Posts mais lidos</h3>
    <!-- Query dos mais lidos -->
    <?php
    $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'orderby'   => 'meta_value_num',
        'meta_key'  => 'post_views_count',
        'order'     => 'DESC',
        'ignore_sticky_posts' => 1,
        'posts_per_page'    => 3,
    );
    $ListsWPQuery = new WP_Query($args);
    if ($ListsWPQuery->have_posts()) :
        while ($ListsWPQuery->have_posts()) : $ListsWPQuery->the_post();
    ?>
        <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
            <div class="post-list row mt-4">
                <div class="col-3 pr-0">
                    <?php if (has_post_thumbnail()) : ?>
                        <img src="<?php echo get_the_post_thumbnail_url($query->ID, 'posts-recentes-blog'); ?>" alt="<?php echo get_the_title(); ?>">
                    <?php else : ?>
                        <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'posts-recentes-blog'); ?>" alt="<?php echo get_the_title(); ?>">
                    <?php endif; ?>
                </div>
                <div class="col-9 content">
                    <h4 class="truncate3"><?php echo get_the_title(); ?></h4>
                </div>
            </div>
        </a>
    <?php endwhile;
    endif; ?>
    <?php wp_reset_postdata(); ?>
</div>