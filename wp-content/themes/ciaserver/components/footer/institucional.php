<?php 
$rodape = get_field('grupo_footer', 'options');
$contatos = get_field('grupo_informacoes_para_contato', 'options'); 
$midias = $contatos['redes_sociais']; ?>
<div class="col-12 col-md-6 col-lg-3">
    <!-- Institucional -->
    <h2><span><?php echo $rodape['titulo_coluna_1']; ?> <?php echo $rodape['destaque_titulo_coluna_1']; ?></span> </h2>
    <p><?php echo $rodape['texto_footer']; ?></p>

    <!-- Redes sociais -->
    <?php if ($midias) : ?>
        <h3><span><?php echo $rodape['titulo_social']; ?><?php echo $rodape['destaque_titulo_redes_sociais']; ?></span></h3>

        <?php foreach ($midias as $key => $midia) : ?>
            <a href="<?php echo $midia['link_social']; ?>" target="_blank" class="midias-rodape"><i class="<?php echo $midia['icone_social']; ?>"></i></a>
        <?php endforeach; ?>
    <?php endif; ?>
</div>