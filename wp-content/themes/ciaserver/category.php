<?php get_header(); 
    $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];
    $term = get_queried_object();
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => 'post',
        'paged'     => $paged,
        'order'     => 'DESC',
        'cat'   => $term->term_id,
    );
    $WPQuery = new WP_Query($args);
?>
<!-- Header padrão -->
<?php get_template_part('components/header/header-padrao'); ?>
<section class="main-blog">
    <div class="container">
        <div class="row">
            <!-- Posts -->
            <div class="col-12 col-lg-8">
                <h2 class="mb-5 title-single">
                    <?php echo $WPQuery->found_posts; ?>
                    <?php _e('Post(s) encontrado(s) na categoria: '); ?> <?php echo $term->name; ?>
                </h2>

                <?php get_template_part('/components/blog/blog'); ?>
            </div>
            <!-- Sidebar -->
            <aside class="col-12 col-lg-3 offset-lg-1">
                <!-- Pesquisar e categoria -->
                <?php dynamic_sidebar( 'sidebar-right' ); ?>
                
                <!-- Mais lidos e banners -->
                <?php get_template_part( '/components/blog/sidebar' ); ?>
            </aside>
        </div>
    </div>
</section>
<?php get_footer(); ?>