<form class="bianca-form p-2 p-md-0" id="contato-completo">
    <div class="row my-0">
        <div class="col-12 col-md-6 px-1 mb-0">
            <div class="form-group mb-1 pb-0">
                <label for="nomeContato" class="sr-only">Nome</label>
                <input type="text" class="form-control" name="nome" id="nomeContato" placeholder="Nome*">
            </div>
        </div>

        <div class="form-group col-12 col-md-6 px-1 mb-0">
            <div class="form-group mb-1 pb-0">
                <label for="telefoneContato" class="sr-only">Telefone</label>
                <input type="text" class="form-control phone" name="tel" id="telContato" placeholder="Telefone*">
            </div>
        </div>
    </div>
    <div class="row my-0">
        <div class="form-group col-12 px-1 my-1">
            <label for="email" class="sr-only">E-mail</label>
            <input type="email" class="form-control" name="email" id="emailContato" placeholder="E-mail*">
        </div>
        <div class="form-group col-12 px-1 my-1">
            <label for="bairro" class="sr-only">Bairro</label>
            <input type="text" class="form-control" name="bairro" id="bairroContato" placeholder="Bairro*">
        </div>
        <div class="form-group col-12 px-1 my-1">
            <label for="cidadeContato" class="sr-only">Cidade</label>
            <input type="text" class="form-control" name="cidade" id="cidadeContato" placeholder="Cidade*">
        </div>
        <div class="col-12 p-1">
            <label for="mensagem" class="sr-only">Mensagem</label>
            <textarea class="form-control" name="mensagem" id="mensagemContato" rows="3" placeholder="Mensagem*"></textarea>
        </div>
        <div class="col-12 p-1">
            <div class="form-group mb-0">
                <button type="button" class="btn btnFormBianca" onclick="enviarMensagem()" id="btnSubmit">Enviar mensagem</button>
            </div>
        </div>
    </div>
</form>

<?php wp_enqueue_script('Contato Completo', get_template_directory_uri() . '/components/formularios/contato-completo.js', array('jquery'), 1, true); ?>