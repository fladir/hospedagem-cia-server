<?php

add_action('init', 'myStartSession', 1);
function myStartSession()
{
  if (!session_id()) {
    session_start();
  }
}

# Adicionando menus ao menu principal do painel adiminstrativo
function admin_bar_item(WP_Admin_Bar $admin_bar)
{
  if (!current_user_can('manage_options')) {
    return;
  }

  $args = array(
    'id' => 'cia_menu',
    'title' => __('<span class="cia-logo"> <img  src="' . get_template_directory_uri() . '/core/assets/img/criacao-de-sites.png"></span>'),
    'href' => 'http://www.ciawebsites.com.br'
  );
  $admin_bar->add_node($args);

  $args = array(
    'id' => 'cia_menu_criacao',
    'parent' => 'cia_menu',
    'title' => __('Criação de Sites'),
    'href' => 'http://www.ciawebsites.com.br/solucoes/criacao-de-sites/'
  );
  $admin_bar->add_node($args);

  $args = array(
    'id' => 'cia_menu_otimizacao',
    'parent' => 'cia_menu',
    'title' => __('Otimização de Sites'),
    'href' => 'http://www.ciawebsites.com.br/solucoes/otimizacao-de-sites-seo/'
  );
  $admin_bar->add_node($args);

  $args = array(
    'id' => 'cia_menu_lojas',
    'parent' => 'cia_menu',
    'title' => __('Lojas Virtuais'),
    'href' => 'http://www.ciawebsites.com.br/solucoes/criacao-de-lojas-virtuais/'
  );
  $admin_bar->add_node($args);
}
add_action('admin_bar_menu', 'admin_bar_item', 500);


# Remover Dashboard widgets
function remove_dashboard_widgets()
{
  global $wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['wpseo-dashboard-overview']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets');

// Customizar o Footer do WordPress
function remove_footer_admin()
{
  echo '© <a href="http://www.ciawebsites.com.br/">Cia Web Sites</a> - Criação e otimização de sites';
}
add_filter('admin_footer_text', 'remove_footer_admin');

# PÁGINA DE LOGIN
function change_title_on_logo()
{
  return 'Voltar para ' . get_bloginfo('name');
}
add_filter('login_headertext ', 'change_title_on_logo');

##########//Remover códigos
//head
remove_action('wp_head', 'rsd_link'); //removes EditURI/RSD (Really Simple Discovery) link.
remove_action('wp_head', 'wlwmanifest_link'); //removes wlwmanifest (Windows Live Writer) link.
remove_action('wp_head', 'wp_generator'); //removes meta name generator.
remove_action('wp_head', 'wp_shortlink_wp_head'); //removes shortlink.
remove_action('wp_head', 'feed_links', 2); //removes feed links.
remove_action('wp_head', 'feed_links_extra', 3);  //removes comments feed.
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_resource_hints', 2);

#Thumbnails
require(get_template_directory() . '/core/functions/thumbnails.php');

#customizer
require (get_template_directory() . '/core/functions/customizer.php');

function navbar_blog_widget()
{
  register_sidebar(array(
    'name'  => 'Sidebar Blog',
    'id'    => 'sidebar-right',
    'before_widget'  => '<div class="sidebar col-12 py-3 mt-4">',
    'after_widget'   => '</div>',
    'before_title'   => '<h3>',
    'after_title'    => '</h3>',
  ));
}
add_action('widgets_init', 'navbar_blog_widget');

# Exibindo a qtde post lido a cada post na lista de posts do blog
function chr_posts_custom_column_views($column_name, $id)
{
  if ($column_name === 'post_views') {
    echo chr_setPostViews(get_the_id());
  }
}
add_action('manage_posts_custom_column', 'chr_posts_custom_column_views', 5, 2);

// Paginando Categoria, Taxonomia é nencessário colocar o nome da slug
function my_post_queries($WPQuery) {
  if (!is_admin() && $WPQuery->is_main_query()) {
//    if (is_tax()) {
//      $WPQuery->set('posts_per_page', 1);
//    }
      if(is_home()){
          $WPQuery->set('posts_per_page', 10);
      }
      if(is_category()) {
          $WPQuery->set('posts_per_page', 10);
      }
  }
}
add_action('pre_get_posts', 'my_post_queries');
