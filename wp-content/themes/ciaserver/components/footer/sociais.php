<?php $rodape = get_field('grupo_footer', 'options'); ?>
<?php $contatos = get_field('grupo_informacoes_para_contato', 'options'); 
$redes = $contatos['redes_sociais'];
?>
    
<?php if($redes) : ?>
    <?php foreach($redes as $key => $rede) : ?>
        <span><a href="<?php echo $rede['link_social']; ?>" target="_blank" class="btn-footer-keke"><i class="<?php echo $rede['icone_social']; ?>"></i></a></span>        
    <?php endforeach; ?>
<?php endif; ?>