<div id="form-rema">
    <div class="container">
        <div class="col-12">
            <div class="row">

                <div class="col-12">
                    <form class="form" id="contato-simples">
                        <div class="row my-0">
                            <div class="col-12 p-1">
                                <div class="form-group mb-1 pb-0">
                                    <label for="nomeContato">Nome:</label>
                                    <input type="text" class="form-control" name="nome" id="nomeContato">
                                </div>
                            </div>
                        </div>

                        <div class="row my-0">
                            <div class="col-12 col-md-6 px-1 mb-0">
                                <div class="form-group mb-1 pb-0">
                                    <label for="telefoneContato">Telefone:</label>
                                    <input type="text" class="form-control" name="tel" id="telefoneContato">
                                </div>
                            </div>

                            <div class="form-group col-12 col-md-6 px-1 mb-0">
                                <div class="form-group mb-1 pb-0">
                                    <label for="emailContato">E-mail:</label>
                                    <input type="email" class="form-control" name="email" id="emailContato">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row my-0">
                            <div class="col-12 p-1">
                                <div class="form-group mb-0 pb-0">
                                    <label for="enderecoContato">Endereço:</label>
                                    <input type="text" class="form-control" name="endereco" id="enderecoContato">
                                </div>
                            </div>
                        </div>

                        <div class="row my-0">
                            <div class="col-12 p-1">
                                <label for="mensagemContato">Mensagem:</label>
                                <textarea class="form-control" name="mensagem" id="mensagemContato" rows="3"></textarea>
                            </div>
                            <div class="col-12 p-1">
                                <div class="form-group mb-0">
                                    <button type="button" class="btn btn-secundario-inverse pull-right" onclick="enviarMensagemSimples()" id="btnSubmit">
                                        <div class="botao-texto">Enviar</div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<?php wp_enqueue_script('ProcessarEnvioContatoSimples', get_template_directory_uri() . '/components/formularios/contato-simples.js', array('jquery'), 1, true); ?>