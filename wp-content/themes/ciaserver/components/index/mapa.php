<?php
$mapa = get_field('grupo_de_configuracoes_gerais', 'options')['link_mapa_maps'];
?>
<?php if($mapa) : ?>
<section class="mapa">
    <div class="box-mapa"><img src="<?php echo bloginfo('template_directory'); ?>/assets/img/location-mapa.png" alt="Mapa" > <h2>Onde estamos</h2></div>
    <div class="acf-map" data-zoom="<?php echo esc_attr($mapa['zoom']); ?>">
        <div class="marker" data-lat="<?php echo esc_attr($mapa['lat']); ?>" data-lng="<?php echo esc_attr($mapa['lng']); ?>"></div>
    </div>
</section>
<?php endif; ?>
