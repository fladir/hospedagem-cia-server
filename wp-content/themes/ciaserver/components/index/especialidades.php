<?php
$img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao'];


$args_page = array(
    'post_type' => 'page',
    'pagename' => 'especialidades',
);
$query = new WP_Query($args_page);

$args = array(
    'post_type' => 'especialidades',
    'orderby'  => 'date',
    'order'    => 'DESC',
    'posts_per_page' => -1,
);
$WPQuery = new WP_Query($args);
?>
<section class="especialidades-home">
    <?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
        <?php $banner = get_field('imagem_inicio'); ?>
        <?php $linkbanner = get_field('link_banner'); ?>
    <!-- Imagem à direita -->
        <?php $imgDecorativa = get_field('imagem_decorativa'); ?>
        <?php if($imgDecorativa) : ?>
            <img class="img-decorativa rellax" data-rellax-speed="2" src="<?php echo wp_get_attachment_image_url($imgDecorativa, 'index-decoracao'); ?>" alt="<?php echo get_the_title(); ?>">
        <?php endif; ?>
        <!-- Imagem à esquerda -->
        <?php $img = get_field('imagem_pagina_inicial'); ?>
        <?php if($img) : ?>
        <img class="img-bg-servico rellax" data-rellax-speed="-4" src="<?php echo wp_get_attachment_image_url($img, 'index-sobre'); ?>" alt="<?php echo get_the_title(); ?>">
        <?php endif; ?>
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex flex-column align-items-center justify-content-center">
                <div class="borda-titulo"></div>
                <h2><?php the_title(); ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="owl-carousel owl-theme" id="especialidades-slide">
                <?php if($WPQuery->have_posts()) : while($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                    <div class="item wow fadeInRightBig">
                        <a href="<?php the_permalink(); ?>" class="hvr-bounce-to-top btn-slide">
                            <figure>
                                <?php if (has_post_thumbnail()) : ?>
                                    <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'especialidades-index'); ?>" alt="<?php echo get_the_title(); ?>" />
                                <?php else : ?>
                                    <img src="<?php echo wp_get_attachment_image_url($img_padrao, 'especialidades-index'); ?>" alt="<?php echo get_the_title(); ?>" />
                                <?php endif; ?>
                            </figure>
                            <div class="title">
                                <h3 class="truncate2"><?php the_title(); ?></h3>
                            </div>
                        </a>
                    </div>
                <?php endwhile; 
                    endif;
                    wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
    <!-- Banner de destaque da seção -->
    <?php if($banner) : ?>
        <?php if($linkbanner) : ?>
            <a href="<?php echo $linkbanner; ?>">
        <?php endif; ?>
            <img class="img-banner" src="<?php echo wp_get_attachment_image_url($banner, 'banner_link'); ?>" alt="<?php echo get_the_title($query->ID); ?>">
        <?php if($linkbanner) : ?>
            </a>
        <?php endif; ?>
    <?php endif; ?>

    <?php endwhile; 
        endif;
        wp_reset_postdata();
    ?>
</section>