<!-- Modal Busca -->
<div class="modal fade" id="modal-busca" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Fale com o corretor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-times"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <?php echo do_shortcode('[contact-form-7 id="206" title="Fale com o corretor"]'); ?>
      </div>
    </div>
  </div>
</div>

<!-- Overlay do Menu -->
<div class="mobile-menu-overlay"></div>

<!-- Formulário de contato -->
<?php if(!is_front_page() && ! is_single()) : ?>
    <?php $args= array(
            'post_type' => 'page',
        'pagename' => 'home',
    );
    $WPQuery = new WP_Query($args);
    ?>
    <?php if($WPQuery->have_posts()) : while($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
    <?php get_template_part('components/index/call-to-action'); ?>
    <?php endwhile; endif; wp_reset_postdata(); ?>
    <?php get_template_part('components/index/mapa'); ?>
<?php endif; ?>

<!-- Footer -->
<footer class="wow fadeIn">
    <div class="container py-footer">
        <div class="row">
              <div class="col-12 my-3 col-md-6 offset-md-3 col-lg-4 offset-lg-0 my-md-0 mb-md-5 mb-lg-0 logo-footer">
                <!-- Logo do rodapé -->
                <?php get_template_part('/components/footer/logo-footer'); ?>
              </div>

              <div class="col-12 col-lg-3 offset-lg-1 text-center text-sm-left my-3 my-md-0 p-0 contatos-footer">

                  <!-- Menu -->
                  <?php get_template_part('/components/footer/menu'); ?>

              </div>

            <div class="col-12 col-lg-3 offset-lg-1 text-center text-sm-left my-3 my-md-0 p-0 contatos-footer">

                <!-- Endereço e demais opções de contato -->
                <?php get_template_part('/components/footer/contatos'); ?>

            </div>


        </div>
    </div>
    <div>
      <!-- Copyright -->
      <?php get_template_part('/components/footer/cia-logo-footer'); ?>
      <!-- Copyright -->
    </div>
</footer>
<!-- Footer -->
<script>
    // Injetando atributos no botão CTA Menu
    document.addEventListener("DOMContentLoaded", () => {
        const btn = document.querySelector("a.nav-link[href='#modal-busca']");
        if(btn){
            btn.setAttribute('data-toggle', 'modal');
        }
    });

</script>
<!-- Tornar os itens do menu que tenha subitens clicáveis sem carregar o link -->
<?php if(wp_is_mobile()) : ?>
  <script>
    document.addEventListener("DOMContentLoaded", () => {
      // Adicionando o dropdown no Menu do topo apenas no mobile
      const tela = window.matchMedia("(max-width: 991px)").matches;
      if (tela) {
        const menu = document.querySelectorAll('#menu-secundario li.menu-item-has-children');
        menu.forEach((item) => {
          let id = item.getAttribute('id')
          let number = id.split('-')
          let link = item.querySelector(`a[id^=menu-item-dropdown-${number[2]}]`)
          link.setAttribute('href', '#')
          link.addEventListener('click', (event) => {
            event.preventDefault()
            let dropdown = item.querySelector(`ul[aria-labelledby=menu-item-dropdown-${number[2]}]`)
            dropdown.classList.toggle('show')
          })
        })
      }
    })
  </script>
<?php endif; ?>
<?php wp_footer(); ?>
</body>

</html>