<?php get_header(); ?>
<?php get_template_part('components/header/header-padrao'); ?>
<section class="page-404">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center pt-5">
                <h2 class="title-home">404</h2>

                <h3>Não encontrou o que procura?</h3>

                <p class="my-5"><a class="btn btn-form" href="<?php echo bloginfo('url'); ?>">Retorne para o início</a></p>
                <h4>Ou confira nossas novidades</h4>
            </div>
        </div>
    </div>
</section>
<?php get_template_part('/components/index/blog'); ?>
<?php get_footer(); ?>