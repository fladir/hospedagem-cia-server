<?php
# BACK END
function load_admin_css()
{
    wp_enqueue_style('style_admin', get_template_directory_uri() . '/core/assets/css/style_admin.css', array());
}

add_action('admin_head', 'load_admin_css');


function load_login_css()
{
    wp_enqueue_style('style_login', get_template_directory_uri().'/core/assets/css/style_login.css');
}
add_action('login_enqueue_scripts', 'load_login_css');

# FRONT END
function pbo_load_css()
{
    wp_register_style('Bootstrap', get_template_directory_uri() . '/assets/libs/bootstrap/css/bootstrap.min.css', array());
    wp_register_style('fontAwesome', get_template_directory_uri() . '/assets/libs/fontawesome-free/css/all.min.css', array());
    wp_register_style('AnimateCSS', get_template_directory_uri() . '/assets/libs/animate-css/animate.min.css', array());
    wp_register_style('owlCarouselCSS', get_template_directory_uri() . '/assets/libs/owl-carousel/assets/owl.carousel.min.css', array());
    wp_register_style('owlCarouselTheme', get_template_directory_uri() . '/assets/libs/owl-carousel/assets/owl.theme.default.min.css', array());
    wp_register_style('fancyBox', get_template_directory_uri() . '/assets/libs/fancybox/jquery.fancybox.min.css', array());
    wp_register_style('hoverCSS', get_template_directory_uri() . '/assets/libs/hover-css/hover-min.css', array());
    wp_register_style('customCSS', get_template_directory_uri() . '/assets/css/style.css', array());

    wp_enqueue_style('Bootstrap');
    wp_enqueue_style('fontAwesome');
    wp_enqueue_style('AnimateCSS');
    wp_enqueue_style('owlCarouselCSS');
    wp_enqueue_style('owlCarouselTheme');
    wp_enqueue_style('fancyBox');
    wp_enqueue_style('hoverCSS');
    wp_enqueue_style('customCSS');

}

add_action('wp_enqueue_scripts', 'pbo_load_css');
