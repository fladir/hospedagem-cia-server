<?php
#Empreendimentos
$args = array(
    'show_in_rest' => true,
    'supports'  => array('title', 'editor', 'thumbnail'),
    'menu_icon' => 'dashicons-schedule',
);

$labels = array(
    'name' => 'Empreendimentos',
    'singular_name' => 'Empreendimento',
    'menu_name' => 'Empreendimentos',
    'add_new' => 'Adicionar novo ' . strtolower('Empreendimento'),
    'add_new_item' => 'Adicionar novo ' . strtolower('Empreendimento'),
    'new_item' => 'Novo ' . strtolower('Empreendimento'),
    'edit_item' => 'Editar ' . strtolower('Empreendimento'),
    'view_item' => 'Vizualizar ' . strtolower('Empreendimento'),
    'all_items' => 'Todos os ' . strtolower('Empreendimentos'),
    'search_items' => 'Pesquisar ' . strtolower('Empreendimentos'),
    'not_found' => 'Nenhum' . strtolower('Empreendimento') . ' foi encontrado',
    'not_found_in_trash' => 'Nenhuma ' . strtolower('Empreendimento') . ' encontrado na lixeira',
    'featured_image' => 'Imagem destacada',

    'set_freatured_image' => 'Escolher como imagem destacada',
    'remove_featured_image' => 'Remover imagem destacada',
    'use_freatured_image' => 'Usar como imagem descatada'

);
$custom_post_type_slide = new pbo_register_custom_post_type('empreendimentos', 'Empreendimentos', $args, $labels);

#  Taxonomia Serviços
$iniciar_taxonomia_produtos = new pbo_register_custom_taxonomy('categoria-empreendimentos', 'Categorias', 'empreendimentos', $args);
