<section id="newsletter">

   <div class="container">

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                <h2 class="h2 text-white">Newsletter</h2>
            </div>
        </div> 
        
        <hr>

        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center text-white">
                Cadastre-se em nossa newsletter e fique por dentro das novidades
            </div>
        </div>

        <form class="row mt-3">
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 text-center text-white">
                <div class="form-group">                  
                  <input type="text" class="form-control" name="nome" id="" placeholder="Digite seu nome">                  
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text-center text-white">
                <div class="form-group">                  
                  <input type="text" class="form-control" name="email" id="" placeholder="Digite seu melhor email">                  
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-2 col-xl-2 text-center text-white">
                <div class="form-group">                  
                  <button type="submit" class="btn btnWhite w-100">Enviar</button>              
                </div>
            </div>
        </form>        
        

   </div>   

</section>

<?php /* Restore original Post Data */ wp_reset_postdata();?>