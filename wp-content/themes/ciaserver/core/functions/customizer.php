<?php
    // Customizações do Menu Personalizar (Tema)
    function customize_ciaprime($wp_customize){
        // Logo alternativa
        $wp_customize->add_setting('alter_logo', array(
            'default'   => '',
            'type'  => 'theme_mod'
        ));
        $wp_customize->add_control(new WP_Customize_Media_Control($wp_customize, 'alter_logo', array(
            'label' => sprintf('Logo alternativa', 'Cia Prime'),
            'section'   => 'title_tagline',
            'mime_type' => 'image',
            'settings' => 'alter_logo',
            'priority'  => 9,
        )
        ));

        // Opções de exibição de logomarca no cabeçalho
        $wp_customize->add_setting('header_logo', array(
            'default' => 'logo',
            'type'  => 'theme_mod',
        ));

        $wp_customize->add_control('header_logo', array(
            'type'  => 'radio',
            'priority' => 10,
            'section'   => 'title_tagline',
            'label' => sprintf('Logo padrão no cabeçalho', 'Cia Prime'),
            'description' => sprintf('Logomarca a ser exibida no Menu do topo', 'Cia Prime'),
            'choices'   => array(
                'logo' => 'Logo padrão',
                'logo-alt' => 'Logo alternativa'
            )
        ));

        // Opções de exibição de logomarca no rodapé
        $wp_customize->add_setting('footer_logo', array(
            'default' => 'logo',
            'type'  => 'theme_mod',
        ));

        $wp_customize->add_control('footer_logo', array(
            'type'  => 'radio',
            'priority' => 11,
            'section'   => 'title_tagline',
            'label' => sprintf('Logo padrão no rodapé', 'Cia Prime'),
            'description' => sprintf('Logomarca a ser exibida no Rodapé', 'Cia Prime'),
            'choices'   => array(
                'logo' => 'Logo padrão',
                'logo-alt' => 'Logo alternativa'
            )
        ));

    }
    add_action('customize_register', 'customize_ciaprime');
?>