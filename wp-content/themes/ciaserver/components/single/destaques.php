<?php $infos_finais = get_field('info_bloco_tres');
if ($infos_finais) :
    $count = 0;
?>
    <div class="container">
        <?php foreach ($infos_finais as $key => $info_final) : ?>
            <div class="info-2cols row align-items-center my-5 <?php echo $count % 2 ? '' : 'flex-md-row-reverse'; ?>">
                <div class="col-12 col-md-6 mask-border">
                    <div class="msk"></div>
                    <div class="clip-interno">
                        <div class="owl-carousel owl-theme" id="catalogo-imagens-second-<?php echo $count; ?>">
                            <?php $slides = $info_final['imagem_destaque_final']; ?>
                            <?php foreach ($slides as $slide) : ?>
                                <img src="<?php echo wp_get_attachment_image_url($slide['banner_destaque_final_vitrine'], 'blog-destaque'); ?>" alt="<?php echo $info_final['titulo_secao_final']; ?>">
                            <?php endforeach; ?>
                        </div>
                        <script>
                            window.addEventListener('DOMContentLoaded', event => {
                                instanciarPostsSlide('#catalogo-imagens-second-<?php echo $count; ?>');
                            });
                        </script>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <h2><span><?php echo $info_final['titulo_secao_final']; ?></span></h2>
                    <p><?php echo $info_final['texto_secao_final']; ?></p>
                </div>
            </div>
            <?php $count++; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>